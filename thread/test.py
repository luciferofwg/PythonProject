#-*- coding: UTF-8 -*- 
import threading
import time
 
exitFlag = 0
 
class myThread (threading.Thread):   #继承父类threading.Thread
	def __init__(self, threadID, name, count, args):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.count = count
		self.args = args
		
	def run(self):				   #把要执行的代码写到run函数里面 线程在创建后会直接运行run函数 
		print "Starting " + self.name
		print_time(self.name, 1, self.count, self.args)
		print "Exiting " + self.name
 
def print_time(threadName, delay, count, args):
	while count:
		if exitFlag:
			threading.Thread.exit()
		time.sleep(delay)
		print "%s: %s" % (threadName, time.ctime(time.time()))
		print 'cout = ' + str(count) + 'args[0]:' + args[0]
		print 'cout = ' + str(count) + 'args[1]:' + args[1]
		print 'cout = ' + str(count) + 'args[2]:' + str(args[2])
		count -= 1

def task():
	threads = []
	#创建线程的数量
	threadNum = 3
	threadName = ['pic_1', 'pic_2', 'pic_3']
	threadAgrs = [ ['pic_1', 'pic_path1', 'pic_url1',1],['pic_2', 'pic_path2', 'pic_url2',2],['pic_3', 'pic_path3', 'pic_url3',3] ]
	
	for index in xrange(0, threadNum):
		# 创建新线程
		time.sleep(0.1)
		thread = myThread(index, 'Thread_' + threadName[index], index*2, threadAgrs[index])
		thread.start()
		threads.append(thread)  
	#等待线程运行结束
	for t in threads:  
		t.join()
	
def main():
	task()
	print "Exiting Main Thread"

main()