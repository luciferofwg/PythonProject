#-*- coding: UTF-8 -*- 
import threading
import time
 
exitFlag = 0
 
class myThread (threading.Thread):   #继承父类threading.Thread
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):                   #把要执行的代码写到run函数里面 线程在创建后会直接运行run函数 
        print "Starting " + self.name
        print_time(self.name, 1, self.counter)
        print "Exiting " + self.name
 
def print_time(threadName, delay, counter):
    while counter:
        if exitFlag:
            threading.Thread.exit()
        time.sleep(delay)
        print "%s: %s" % (threadName, time.ctime(time.time()))
        counter -= 1
def task():
	threads = []
	for index in xrange(1, 10):
		# 创建新线程
		time.sleep(0.1)
		thread = myThread(index, 'Thread_' + str(index), index*1000)
		thread.start()
		threads.append(thread)  
	#等待线程运行结束
	for t in threads:  
		t.join()
	
def main():
	task()
	print "Exiting Main Thread"

main()