# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CitySpiderItem(scrapy.Item):
    # define the fields for your item here like:
    # 当前页的url
    url = scrapy.Field()
    # 标题
    title = scrapy.Field()
    # 更新信息
    update_info = scrapy.Field()
    # 
    # last_updated = scrapy.Field(serializer=str)

    price = scrapy.Field()

    house_type = scrapy.Field()

    community_info = scrapy.Field()

    phone_num = scrapy.Field()

    Overview_info = scrapy.Field()

    description = scrapy.Field()

    prices = scrapy.Field()

    aroud_evn = scrapy.Field()

    house_type_Img = scrapy.Field()

    last_updated = scrapy.Field(serializer=str)