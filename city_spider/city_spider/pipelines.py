# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
from scrapy.conf import settings

class CitySpiderPipeline(object):
    def process_item(self, item, spider):
        return item


class mongoPipeline(object):
    # 打开数据库
    def __init__(self):
        self.client = pymongo.MongoClient(host=settings['MONGO_HOST'], port=settings['MONGO_PORT'])
        self.db = self.client[settings['MONGO_DB']]  # 获得数据库的句柄
        self.coll = self.db[settings['MONGO_COLL']]  # 获得collection的句柄

    # 对数据进行处理
    def process_item(self, item, spider):
        item = dict(item)
        self.coll.insert(item)  # 向数据库插入一条记录
        return item