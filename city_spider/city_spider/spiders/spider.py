# coding: utf-8
import re
import scrapy
from bs4 import BeautifulSoup
from scrapy.http import Request
from scrapy.loader import ItemLoader

from items import CitySpiderItem as cityItem

debug_ = True

class MySpider(scrapy.Spider):
    # 二手房url
    urls = []
    urls.append('http://xa.58.com/ershoufang/')
    urls.append('http://xa.58.com/zufang/')
    name = "city_spider"

    def start_requests(self):
        for url in self.urls:
            yield Request(url, self.traverse_pages)

    def traverse_pages(self, response):
        # 获取当前页面的最大页
        pager_div = BeautifulSoup(response.text, 'lxml').find('div',attrs={'class':'pager'})
        if pager_div is not None:
            max_num = pager_div.find_all('span')[-2].get_text()
            if pager_div is not None:
                max_num = '3'
            for num in range(1, int(max_num)):
                url = str(response.url) + 'pn' + str(num) + '/'
                yield Request(url, callback=self.prase_page_url, meta={'url':str(response.url)})
                if debug_ is True:
                    break

    def prase_page_url(self, response):
        l =  ItemLoader(item=cityItem(), response=response)
        ul_house_list = BeautifulSoup(response.text, 'lxml').find('ul',attrs={'class':'house-list-wrap'})
        if ul_house_list is not None:
            lis =  ul_house_list.find_all('li')
            if lis is not None:
                for li in lis:
                    url = response.meta['url'] + str(li.get('logr').split('_')[3]) + 'x.shtml'
                    l.add_value('url', url)
                    # l.load_item()
                    yield Request(url, callback=self.parse_main_page)
                    if debug_ is True:
                        break

        # return 

    def parse_main_page(self, response):
        l =  ItemLoader(item=cityItem(), response=response)
        bsobj = BeautifulSoup(response.text, 'lxml').find('div',attrs={'class':'main-wrap'})
        if bsobj is not None:
            # 房源基本信息
            house_title = bsobj.find('div',attrs={'class':'house-title'})
            title,update_info = self.parse_title(house_title)
            
            # l.add_value('title', title)
            # l.add_value('update_info', update_info)

            # 房源详细信息
            house_basic = bsobj.find('div',attrs={'class':'house-basic-info clearfix'})
            self.parse_basic(house_basic,response)

            # 处理其他信息
            detail_right = bsobj.find('div',attrs={'class':'house-detail-right'})
            self.parse_detail(detail_right,response)

        # return l.load_item()

    def parse_title(self, house_title):
        if house_title is None:
            return None, None
        # 题目
        title = house_title.find('h1').get_text()
        # 更新信息
        update_info = house_title.find('p').get_text()
        return title, update_info

    def parse_basic(self, house_basic,response):
        if house_basic is None:
            return None, None
        l =  ItemLoader(item=cityItem(), response=response)

        basic_right = house_basic.find('div',attrs={'class':'house-basic-right fr'})
        '''价格'''
        price_info = []
        price = basic_right.find('p',attrs={'class':'house-basic-item1'}).get_text().split(" ")[0]
        # print "".join(price.split())
        # for info in price.split():
        #     price_info.append(info)

        l.add_value('price', price_info)
  

        '''户型'''
        house_type = []
        house_struct = basic_right.find('div',attrs={'class':'house-basic-item2'}).get_text().split(" ")[0]
        # for info in house_struct.split():
        #     house_type.append(info)

        l.add_value('house_type', house_type)

        '''小区信息'''
        community_info = []
        community_lis = basic_right.find('ul',attrs={'class':'house-basic-item3'}).find_all('li')
        for li in community_lis:
            tmp = ''
            spans = li.find_all('span')
            for span in spans:
                for sub in span.get_text().split():
                    if u'地图' != sub:
                        tmp += sub
            community_info.append(tmp)

        # for tmp in community_info:
        #     print tmp
        l.add_value('community_info', community_info)

        '''经纪人电话'''
        phone_num = basic_right.find('div',attrs={'class':'house-chat-entry clearfix'}).find('p',attrs={'class':'phone-num'}).get_text()
        l.add_value('phone_num', phone_num)

    def parse_detail(self,house_detail,response):
        l =  ItemLoader(item=cityItem(), response=response)

        ''' 概况 '''
        Overview_info = []
        div1 = house_detail.find('div',attrs={'class':'general-item general-situation'})
        if div1 is not None:
            div2 = div1.find('div',attrs={'class':'general-item-wrap'})
            if div2 is not None:
                # left
                item_left = div2.find('ul',attrs={'class':'general-item-left'})
                lis = item_left.find_all('li')
                for li in lis:
                    tmp = ''
                    for sub in li.get_text().split():
                       tmp += sub
                    Overview_info.append(tmp)

                # right
                item_right = div2.find('ul',attrs={'class':'general-item-right'})
                lis = item_right.find_all('li')
                for li in lis:
                    tmp = ''
                    for sub in li.get_text().split():
                        tmp += sub
                    Overview_info.append(tmp)
        # print '---------------------------------------\n概况：'
        # for sub in Overview_info:
        #     print sub
        # print '---------------------------------------'
        l.add_value('Overview_info', Overview_info)


        ''' 描述 '''
        description = []
        div = house_detail.find('div',attrs={'class':'general-item general-desc'}).find('div',attrs={'class':'general-item-wrap'})
        if div is not None:
            divs = div.find_all('div')
            for sub_div in divs: 
                if  sub_div is not None:
                    sub_info = {}
                    sub_title = sub_div.find('span',attrs={'class':'pic-desc-item'}).get_text()
                    sub_describetion = "".join(sub_div.find('p').get_text().split())
                    sub_info[sub_title] = sub_describetion

                    description.append(sub_info)
        # print '描述：'
        # for des in description:
        #     for k in des:
        #         print k,':',des[k]
        # print '---------------------------------------'
        l.add_value('description', description)


        ''' 费用 '''
        prices = []
        div = house_detail.find('div',attrs={'class':'general-item general-expense'}).find('div',attrs={'class':'general-item-wrap'})
        if div is not None:
            uls = div.find_all('ul')
            for ul in uls: 
                if  ul is not None:
                    lis = ul.find_all('li')
                    t_c = {}
                    for li in lis:
                        title = li.find_all('span')[0].get_text()
                        title = "".join(title.split())
                        content = li.find_all('span')[1].get_text()
                        content = "".join(content.split())
                        t_c[title] = content
                        # print title,':',content
                    prices.append(t_c)
        # print '费用:'
        # for des in prices:
        #     for k in des:
        #         print k,':',des[k]
        # print '---------------------------------------'
        l.add_value('prices', prices)


        ''' 户型图 '''
        house_type_Img = []
        div = house_detail.find('div',attrs={'class':'general-item general-type'}).find('div',attrs={'class':'general-item-wrap'})
        if div is not None:
            imgs = div.find('ul').find_all('img')
            for img in imgs:
                house_type_Img.append(img.attrs['data-src'])
        # print '图片url:'
        # for url in house_type_Img:
        #     print 'url=',url
        # print '---------------------------------------'
        l.add_value('house_type_Img', house_type_Img)


        ''' 小区周边及环境 '''
        aroud_evn = []
        xq_name = ''
        div = house_detail.find('div',attrs={'class':'general-item xiaoqu-wrap'}).find('div',attrs={'class':'general-item-wrap'})
        if div is not None:
            xq_infos = div.find('div', attrs={'class':'xiaoqu-info'})
            if xq_infos is not None:
                xq_name = xq_infos.find('h3', attrs={'class':'xiaoqu-name'}).get_text()
                xq_desc = xq_infos.find('ul', attrs={'class':'xiaoqu-desc'})
                lis = xq_desc.find_all('li')
                t_c = {}
                for li in lis:
                    title = li.find_all('span')[0].get_text()
                    title = "".join(title.split())

                    content = li.find_all('span')[1].get_text()
                    content = "".join(content.split())
                    t_c[title] = content
                aroud_evn.append(t_c)

        # print '小区信息:'
        # print 'xq_name:'+xq_name
        # for sub in aroud_evn:
        #     for k in sub:
        #         print k,':',sub[k]
        # print '---------------------------------------'
        l.add_value('aroud_evn', aroud_evn)

        return l.load_item()