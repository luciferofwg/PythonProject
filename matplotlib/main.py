#!/usr/bin/env python
# coding: UTF-8

# import numpy as np
# import matplotlib.pyplot as plt
# from pylab import *
# x = np.arange(-5.0, 5.0, 0.02)
# y1 = np.sin(x)
# title("a strait line") 
# plt.figure(1)
# plt.subplot(211)
# plt.plot(x, y1)
# xlabel("x value")
# ylabel("y value")
# plt.subplot(212)
# #设置x轴范围
# xlim(-2.5, 2.5)
# #设置y轴范围
# ylim(-1, 1)
# plt.plot(x, y1)
# xlabel("x value")
# ylabel("y value")
# plt.show()
# plt.savefig("matplot_sample.jpg")



# import matplotlib.pyplot as plt
# labels='frogs','hogs','dogs','logs'
# sizes=15,20,45,10
# colors='yellowgreen','gold','lightskyblue','lightcoral'
# explode=0,0.1,0,0
# # 饼图
# plt.pie(sizes,explode=explode,labels=labels,colors=colors,autopct='%1.1f%%',shadow=True,startangle=80)
# plt.axis('equal')
# # savefig("demo.jpg")
# plt.show()



# import numpy as np
# import matplotlib.pyplot as plt
# # evenly sampled time at 200ms intervals
# t = np.arange(0., 5., 0.1)
# # red dashes, blue squares and green triangles
# # 图像叠加显示
# plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
# plt.grid(True)
# plt.show()


# import numpy as np
# import matplotlib.pyplot as plt
# mu, sigma = 100, 15
# x = mu + sigma * np.random.randn(10000)
# # 数据的直方图
# n, bins, patches = plt.hist(x, 50, normed=1, facecolor='b', alpha=0.75)
# plt.xlabel('Smarts')
# plt.ylabel('Probability')
# #添加标题
# plt.title('Histogram of IQ')
# #添加文字
# plt.text(60, .025, r'$mu=100, sigma=15$')
# plt.axis([40, 160, 0, 0.03])
# #网格线
# plt.grid(True)
# plt.show()


# # 导入 matplotlib 的所有内容（nympy 可以用 np 这个名字来使用）
# from pylab import *
# import numpy as np
# # 创建一个 8 * 6 点（point）的图，并设置分辨率为 80
# figure(figsize=(8,6), dpi=80)
# # 创建一个新的 1 * 1 的子图，接下来的图样绘制在其中的第 1 块（也是唯一的一块）
# subplot(1,1,1)
# X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
# C,S = np.cos(X), np.sin(X)
# # 绘制余弦曲线，使用蓝色的、连续的、宽度为 1 （像素）的线条
# plot(X, C, color="blue", linewidth=1.0, linestyle="-")
# # 绘制正弦曲线，使用绿色的、连续的、宽度为 1 （像素）的线条
# plot(X, S, color="r", lw=4.0, linestyle="-")
# plt.axis([-4,4,-1.2,1.2])
# # 设置轴记号
# xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
#        [r'$-pi$', r'$-pi/2$', r'$0$', r'$+pi/2$', r'$+pi$'])
# yticks([-1, 0, +1],
#        [r'$-1$', r'$0$', r'$+1$'])
# # 在屏幕上显示
# show()



# Import necessary packages
import pandas as pd
# %matplotlib inline
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from sklearn import datasets
from sklearn import linear_model
import numpy as np
# Load data
boston = datasets.load_boston()
yb = boston.target.reshape(-1, 1)
Xb = boston['data'][:,5].reshape(-1, 1)
# Plot data
plt.scatter(Xb,yb)
plt.ylabel('value of house /1000 ($)')
plt.xlabel('number of rooms')
# Create linear regression object
regr = linear_model.LinearRegression()
# Train the model using the training sets
regr.fit( Xb, yb)
# Plot outputs
plt.scatter(Xb, yb,  color='black')
plt.plot(Xb, regr.predict(Xb), color='blue',linewidth=3)
plt.show()