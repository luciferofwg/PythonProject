#-*- coding: UTF-8 -*- 
import urllib
import urllib2
import re
import chardet
import sys
import os
import utils
import gzip

#处理页面标签类
class Tool:
	#去除img标签,7位长空格
	removeImg = re.compile('<img.*?>| {7}|')
	#删除超链接标签
	removeAddr = re.compile('<a.*?>|</a>')
	#把换行的标签换为\n
	replaceLine = re.compile('<tr>|<div>|</div>|</p>')
	#将表格制表<td>替换为\t
	replaceTD= re.compile('<td>')
	#把段落开头换为\n加空两格
	replacePara = re.compile('<p.*?>')
	#将换行符或双换行符替换为\n
	replaceBR = re.compile('<br><br>|<br>')
	#将其余标签剔除
	removeExtraTag = re.compile('<.*?>')
	def replace(self,x):
		x = re.sub(self.removeImg,"",x)
		x = re.sub(self.removeAddr,"",x)
		x = re.sub(self.replaceLine,"\n",x)
		x = re.sub(self.replaceTD,"\t",x)
		x = re.sub(self.replacePara,"\n	",x)
		x = re.sub(self.replaceBR,"\n",x)
		x = re.sub(self.removeExtraTag,"",x)
		#strip()将前后多余内容删除
		return x.strip()



class BDTB:
	#初始化，传入基地址，是否只看楼主的参数
	def __init__(self,baseUrl,seeLZ,floorTag):
		self.baseURL = baseUrl
		self.seeLZ = '?see_lz='+str(seeLZ)
		#HTML标签剔除工具类对象
		self.tool = Tool()
		#全局file变量，文件写入操作对象
		self.file = None
		#楼层标号，初始为1
		self.floor = 1
		#默认的标题，如果没有成功获取到标题的话则会用这个标题
		self.defaultTitle = u"百度贴吧"
		#是否写入楼分隔符的标记
		self.floorTag = floorTag
	
	#解压缩gzip数据
	def unZipData(self, data):
		path = os.getcwd()
		fileName = path + '\\' + 'temp.dat'
		print fileName
		utils.writeData(fileName, data, 'wb')
		f = gzip.open(fileName, 'rb')
		html = f.read()
		#f.close()
		#删除临时文件
		#os.remove(fileName)
		return html
			
	#传入页码，获取该页帖子的代码
	def getPage(self,pageNum):
		try:
			url = self.baseURL+ self.seeLZ + '&pn=' + str(pageNum)
			print "getPage()::" + url
			req_header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
							'Accept':'text/html;q=0.9,*/*;q=0.8',
							'Accept-Charset':'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
							'Accept-Encoding':'gzip',
							'Connection':'close',
							'Referer':None #注意如果依然不能抓取的话，这里可以设置抓取网站的host
							}
			time_out = 5
			request = urllib2.Request(url, None, req_header)
			response = urllib2.urlopen(request, None, time_out)
			#返回UTF-8格式编码内容
			html = response.read()
			data = self.unZipData(html)
			return data.decode('utf-8')

		except urllib2.URLError, e:
			if hasattr(e,"reason"):
				print u"连接百度贴吧失败,错误原因",e.reason
				return None
	
	#获取帖子标题
	def getTitle(self,page):
		#得到标题的正则表达式
		pattern = re.compile('<h1 class="core_title_txt.*?>(.*?)</h1>',re.S)
		result = re.search(pattern,page)
		if result:
			#如果存在，则返回标题
			return result.group(1).strip()
		else:
			return None
	
	 #获取帖子一共有多少页
	def getPageNum(self,page):
		#获取帖子页数的正则表达式
		pattern = re.compile('<li class="l_reply_num.*?</span>.*?<span.*?>(.*?)</span>',re.S)
		result = re.search(pattern,page)
		if result:
			return result.group(1).strip()
		else:
			return None
			
			
	#获取每一层楼的内容,传入页面内容
	def getContent(self,page):
		#匹配所有楼层的内容
		pattern = re.compile('<div id="post_content_.*?>(.*?)</div>',re.S)
		items = re.findall(pattern,page)
		contents = []
		for item in items:
			#将文本进行去除标签处理，同时在前后加入换行符
			content = "\n"+self.tool.replace(item)+"\n"
			contents.append(content.encode('utf-8'))
		return contents
 
	def setFileTitle(self,title):
		#如果标题不是为None，即成功获取到标题
		path = 'D:\\Code\\PythonProject\\Scrapy\\'
		if title is not None:
			self.file = open(path + title + ".txt","w+")
		else:
			self.file = open(path + self.defaultTitle + ".txt","w+")

			
	def writeData(self,contents):
		#向文件写入每一楼的信息
		for item in contents:
			if self.floorTag == '1':
				#楼之间的分隔符
				floorLine = "\n" + str(self.floor) + u"-----------------------------------------------------------------------------------------\n"
				self.file.write(floorLine)
			self.file.write(item)
			self.floor += 1
 
	def start(self):
		print "start():getPage() begin"
		indexPage = self.getPage(1)
		print "start():getPage() end"
		
		print "start():getPageNum() begin"
		pageNum = self.getPageNum(indexPage)
		print "start():getPageNum() end, pageNum=" + str(pageNum)
		
		title = self.getTitle(indexPage)
		self.setFileTitle(title)
		if pageNum == None:
			print u"URL已失效，请重试"
			return
		try:
			print u"该帖子共有" + str(pageNum) + u"页"
			for i in range(1,int(pageNum)+1):
				print u"正在写入第" + str(i) + u"页数据"
				page = self.getPage(i)
				contents = self.getContent(page)
				self.writeData(contents)
		#出现写入异常
		except IOError,e:
			print u"写入异常，原因" + e.message
		finally:
			print u"写入任务完成"
			
def main():
	print u"请输入帖子代号"
	baseURL = 'http://tieba.baidu.com/p/5010497894'
	#baseURL = 'http://tieba.baidu.com/p/' + str(raw_input(u'http://tieba.baidu.com/p/'))
	seeLZ = 0#raw_input(unicode("是否只获取楼主发言，是输入1，否输入0\n", "utf-8").encode("gbk"))
	floorTag = 1#raw_input(unicode("是否写入楼层信息，是输入1，否输入0\n", "utf-8").encode("gbk"))
	bdtb = BDTB(baseURL,seeLZ,floorTag)
	bdtb.start()

main()