#-*- coding: UTF-8 -*- 
import os
import os.path
import shutil
from random import Random

import sys,time


#获取当前文件绝对路径
def getCurPath():
	return os.getcwd()

#测试打印
def printTable(Tab):
	for it in Tab:
		print it

#读取数据
def readData(filePath, mode):
	fo = open(filePath, mode)
	try:
		data = fo.read()
	finally:
		fo.close()
	return data

#存储文本
def writeData(filePath, data, mode):
	fo = open(filePath, mode)
	try:
		fo.write(data)
		fo.close()
	finally:
		fo.close()

#随机生成字符串
def getRandomName(randomlength=12):
	str = ''
	chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
	length = len(chars) - 1
	random = Random()
	for i in range(randomlength):
		str+=chars[random.randint(0, length)]
	return str

#创建文件夹
def mkdirFolde(path, foldeName):
	pathEx = path + '\\' + foldeName
	isExist = os.path.exists(pathEx)
	if not isExist:
		#logger.info('Folde #' + foldeName + '# create success')
		os.makedirs(pathEx)

	return pathEx

#清除之前文件和文件件
def clearFiles(top):
	for root, dirs, files in os.walk(top, topdown=False):
		for name in files:
			if -1 == name.find(".py", 0) and -1 == name.find(".jpg", 0) and -1 == name.find(".pyc", 0):
				os.remove(os.path.join(root, name))				
		for name in dirs:
			if -1 !=name.find("dat"):
				os.rmdir(os.path.join(root, name))
	
#进度条显示
def showBarBegin():
	myLog.write('i', '\rProcess [0%%] |#')
def showBarBegin(num=1, sum=100, bar_word="#"):
	time.sleep(0.1)
	rate = float(num) / float(sum)
	rate_num = int(rate * 100)
	print '\rProcess [%d%%] |' %(rate_num),
	for i in range(0, num):
		os.write(1, bar_word)
	sys.stdout.flush()
def showBarEnd():
	os.write(1, '|')
	sys.stdout.flush()
