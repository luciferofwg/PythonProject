# coding: utf-8
import scrapy
from scrapy.spiders import CrawlSpider
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector
from scrapy.http import Request

from items import NovelItem as Item

import re

class MySpider(CrawlSpider):
    start_url_domain = 'txt53.com'
    name = 'novel'
    allowed_domains = ['txt53.com']
    start_urls = ['http://www.txt53.com/']

    # 重写start_requests函数
    def start_requests(self):
        sub_urls = ['xuanhuan','wuxiaxianxia','dushi','wangyoujingji2','yanqing','kehuanxiaoshuo','xuanyituili','qita']
        for sub in sub_urls:
            new_url = 'http://' + self.allowed_domains[0] + '/html/' + sub
            yield Request(new_url, self.parse, dont_filter=True)
            # break

    def parse(self, response):
        try:
            selector = Selector(response)
            url = selector.xpath('//div[@class="yemian"]//li/a/@href').extract()[1]
            sub_url = re.search(r'(list_\d+_)',url)
            max_page = selector.xpath('//span[@class="pageinfo"]//strong/text()').extract()[1]
            max_page = '10'
            
            for index in range(1, int(max_page)):
                print 'index=', index, 'url=',response.url
                new_url = response.url + '/' + sub_url.group() + str(index) + '.html'
                yield Request(new_url, self.parse_page, dont_filter=True)
        except Exception as e:
            pass

    def parse_page(self, response):
        selector = Selector(response)
        try:
            urls = selector.xpath('//div[@class="xiashu"]//ul//li[@class="qq_g"]//a/@href')
            for url in urls:
                try:
                    new_url = url.extract()
                    yield Request(new_url, self.parse_detail, dont_filter=True)
                except Exception as e:
                    pass        
        except Exception as e:
            pass

    def parse_detail(self, response):
        try:
            item = Item()
            selector = Selector(response)
            name = selector.xpath('//div[@class="shuji"]//li//b/text()').extract()[0]
            type = selector.xpath('//div[@class="shuji"]//li/text()').extract()[1].split(u'：')[1]
            author = selector.xpath('//div[@class="shuji"]//li//a/text()').extract()[0]
            uploader = selector.xpath('//div[@class="shuji"]//li//a/text()').extract()[1]
            source = selector.xpath('//div[@class="shuji"]//li//a/text()').extract()[2]
            size = selector.xpath('//div[@class="shuji"]//li/text()').extract()[5].split(u'：')[1]
            update_time = selector.xpath('//div[@class="shuji"]//li/text()').extract()[6].split(u'：')[1]
            down_tims = selector.xpath('//div[@class="shuji"]//li/text()').extract()[7].split(u'：')[1]
            url = selector.xpath('//div[@class="downbox"]//a/@href').extract()[0]
            
            item['name'] = name
            item['type'] = type
            item['author'] = author
            item['uploader'] = uploader
            item['source'] = source
            item['size'] = size
            item['update_time'] = update_time
            item['down_tims'] = down_tims
            item['url'] = url
            yield scrapy.Request(url=url, callback=self.parse_download,meta={'item':item}, dont_filter=True)
        except Exception as e:
            pass
    def parse_download(self, response):
        try:
            item = Item()
            selector = Selector(response)
            down_load_url = selector.xpath('//div[@class="shuji"]//li//a/@href').extract()[1]
            item = response.meta['item']
            item['url'] = down_load_url
            yield item
        except Exception as e:
            pass
        