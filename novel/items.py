# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NovelItem(scrapy.Item):
    name= scrapy.Field()
    type= scrapy.Field()
    author= scrapy.Field()
    uploader= scrapy.Field()
    source= scrapy.Field()
    size= scrapy.Field()
    update_time= scrapy.Field()
    down_tims= scrapy.Field()
    url= scrapy.Field()
