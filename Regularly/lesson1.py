#-*- coding: UTF-8 -*- 
import re
import urllib
import urllib2
import os
import shutil
import os.path
import stat
from random import Random

#测试打印
def printTable(Tab):
	for it in Tab:
		print it

#读取数据
def readData(filePath):
	fo = open(filePath, "r")
	try:
		data = fo.read()
	finally:
		fo.close()
	return data

#存储文本
def writeData(filePath, data):
	fo = open(filePath, "a")
	try:
		fo.write(data+"\n")
	finally:
		fo.close()

#获取标题
def getTitle(data):
	pattern = re.compile('<h1 class="core_title_txt.*?>(.*?)</h1>', re.S)
	pattLabel = re.compile('<h1 class=.*?>|</h1>')
	result = re.search(pattern, data)
	if result:
		title = re.sub(pattLabel, "", result.group(0))
		return title
	else:
		return None
	
#获取文本
def getContext(data):
	pattern = re.compile('<cc>(.*?)</cc>')
	results = re.findall(pattern, data)

	contents = []
	for result in results:
		item = removeLabel(result)
		contents.append(item)
	return contents
		
#获取图片url
def getImgUrl(data):
	pattern = re.compile('<img class="BDE_Image".*?src="(.*?.jpg)')
	results = re.findall(pattern, data)

	contents = []
	for result in results:
		contents.append(result)
	return contents

#随机生成字符串
def getRandomName(randomlength=8):
	str = ''
	chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
	length = len(chars) - 1
	random = Random()
	for i in range(randomlength):
		str+=chars[random.randint(0, length)]
	return str

#创建文件夹
def mkdirFolde(path, foldeName):
	pathEx = path + foldeName
	isExist = os.path.exists(pathEx)
	if not isExist:
		os.makedirs(pathEx)

	return pathEx + "\\"

#根据图片url下载图片
def downloadImg(imgPath, imgURL):
	try:
		urlopen=urllib.URLopener()
		fp = urlopen.open(imgURL)
		data = fp.read()
		fp.close()
		
		#创建文件夹
		path = mkdirFolde(imgPath, "temp")
		path1 = path + getRandomName() + ".jpg"
		print path1
		
		file = open(path + getRandomName() + ".jpg", "w+b")
		file.write(data)
		file.close()
	except IOError, error:
		print "download error: %s error %s" % (imgURL, error)
	except Exception, e:
		print "Exception :" + e

#清除之前文件和文件件
def clearFiles():
	top = "D:\\Code\\PythonProject\\Regularly"
	for root, dirs, files in os.walk(top, topdown=False):
		for name in files:
			if -1 == name.find(".py", 0) and -1 == name.find("data.dat", 0):
				os.remove(os.path.join(root, name))				
		for name in dirs:
			os.rmdir(os.path.join(root, name))

#代码主体
def main():	
	filePathR = "G:\\GitHub\\PythonProject\\Regularly\\data.dat"
	filePathW = "G:\\GitHub\\PythonProject\\Regularly\\Context.txt"
	filePathI = "G:\\GitHub\\\\PythonProject\\Regularly\\"
	
	clearFiles()
	
	data = readData(filePathR)
	writeData(filePathW, getTitle(data))

	results = getContext(data)
	for result in results:
		writeData(filePathW, result)

	results = getImgUrl(data)
	for imgUrl in results:
		downloadImg(filePathI, imgUrl)
		
if __name__ == "__main__":
    main()
else:
    print "called from intern."


