#!/usr/bin/python
#-*- coding: UTF-8 -*-

import re
import os
import shutil
'''

'''

def Calculate(file_path,flag=None):
	print '\ndeal file ' + file_path + '...'
	numReg = re.compile(r'cost=(.*?).*ms', re.S)
	rmvReg =re.compile(r'cost|ms|=', re.S)

	'''
	# 计算最大值，最小值，均值
	# 统计大于 50ms的数据的百分比
	'''
	max_number = 0.
	min_number = 10000.
	avg_number = 0.
	abo_number = 0.

	total_num = 0.
	count_current = 0
	Threshold = 5

	if flag == 'match':
		Threshold = 10 # 10ms
	elif flag == 'getdata':
		Threshold = 50 # 50ms

	read = open(file_path)
	line=read.readline()
	while line:
			result = re.search(numReg, line)
			if result is not None:
				result = re.sub(rmvReg, '', result.group(0))
				number = float(result)

				count_current+=1

				# 统计最大值，最小值
				if max_number <= number:
					max_number = number
				elif min_number >= number:
					min_number = number
				elif Threshold <= number:#大于Thresholdms
					abo_number+=1
				# 所有的值的和
				total_num += number

				line=read.readline()#如果没有这行会造成死循环
	read.close
	if count_current == 0:
		count_current = 1
	return  round(max_number,3), \
	        round(min_number,3), \
	        round(total_num/count_current,3), \
	        round(abo_number,3), \
	        round(count_current,3),\
	        round( (abo_number/(count_current))*100,3)


def dealCurDirs(file_dir):
	
	# 打开文件
	
	match_log = os.getcwd() + '/match.log'
	if os.path.exists(match_log):
		os.remove(match_log)
	file_match = open(match_log, 'w')
	getdata_log = os.getcwd() + '/getdata.log'
	if os.path.exists(getdata_log):
		os.remove(getdata_log)
	file_getdata = open(getdata_log, 'w')
			
	for root, dirs, _files in os.walk(file_dir):
		for dir in dirs:
			print '------------------------------------------'
			print 'deal current dir:' + dir
			for file in os.listdir(dir):
				# 预处理文本
				if 'ade_nps.log' in file:
					src_path = dir + '/' + file
					dst_path_match = dir + '/match.tmp'
					dst_path_getdata = dir + '/getData.tmp'

					string_cmd_match = "grep 'statistics-time Match: cost=' " + src_path + " > "+dst_path_match
					string_cmd_getdata = "grep 'statistics-time GetData:' " + src_path + " > "+dst_path_getdata
					# print 'grep match ' + src_path
					os.system(string_cmd_match)
					# print 'grep getdata ' + src_path
					os.system(string_cmd_getdata)
					
					#处理文件
					max1, min1, avg1, up50count1, totalcount1, up50per1 = Calculate(dst_path_match, 'match')
					out_match = "max=%.3f,    min=%.3f,    avg=%.3f,    up50count=%.3f,    totalcount=%.3f,    up50ms=%.3f"%(max1,min1,avg1,up50count1,totalcount1,up50per1)
					#out_match = 'max='+str(max1)+',    min='+str(min1)+',    avg='+str(avg1)+',    up5count='+str(up50count1)+',    totalcount='+str(totalcount1)+',    up5ms='+str(up50per1)+'%'
					out_match = out_match + '\n'
					print out_match
					file_match.write(out_match)
					
					# print 'max='+max1+',    min='+min1+',    avg='+avg1+',    up5ms='+up50per1+'%'
					max2, min2, avg2, up50count2, totalcount2, up50per2 = Calculate(dst_path_getdata,'getdata')
					out_getdata = "max=%.3f,    min=%.3f,    avg=%.3f,    up50count=%.3f,    totalcount=%.3f,    up50ms=%.3f"%(max2,min2,avg2,up50count2,totalcount2,up50per2)
					#out_getdata = 'max='+str(max2)+',    min='+str(min2)+',    avg='+str(avg2)+',    up50count='+str(up50count2)+',    totalcount='+str(totalcount2)+',    up50ms='+str(up50per2)+'%'
					out_getdata = out_getdata + '\n'
					print out_getdata
					file_getdata.write(out_getdata)
					
					
					# print 'max='+max2+',    min='+min2+',    avg='+avg2+',    up50ms='+up50per2+'%'
	
	file_match.close()
	file_getdata.close()
	


'''
# 创建文件夹
'''
def mkdirFolde(path, foldeName):
	pathEx = path + foldeName
	isExist = os.path.exists(pathEx)
	if not isExist:
		os.makedirs(pathEx)
	return pathEx

'''
# 准备数据
'''
def prepare(file_dir):
	for file in os.listdir(file_dir):
		# 直接解压gz文件
		if '.gz' in file:
			gz_string = 'gunzip ' + file
			os.system(gz_string)
			
	for file in os.listdir(file_dir):
		# 处理数据
		if 'ade_nps.log' in file:
			name = re.sub("\D", "", file)
			new_path = mkdirFolde(file_dir, name) +'/'+file
			old_path = os.getcwd() + '/' + file
			print 'new_path='+new_path
			print 'old_path='+old_path
			shutil.copyfile(old_path,new_path)
			os.remove(old_path)

file_path = os.getcwd() + '/'
prepare(file_path)
dealCurDirs(file_path)
