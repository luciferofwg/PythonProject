#coding=utf-8

import weather
import time
import weChat
import datetime

# 3小时
# span = 3*3600
# 1s
span = 10
def packData(flag,weas):
    return weather.formatWeather(weas[flag])

def fun_get_weather(weChat,markFriend,url,strTime=None):
    now = datetime.datetime.today()
    time_str = now.strftime('%H:%M')
    clock_zero = now.strftime('%M')
    flag = -1
    if time_str > "20:00" and clock_zero=="00":
        # 明天的天气
        flag = 1
    elif time_str < "12:00" and clock_zero=="00":
        # 今天的天气
        flag = 0
    else:
        return

    soup = weather.getDoc(url)
    if soup is not None:
        # 获取最近7天的天气情况
        weas = weather.getWeather(soup)
        msg = packData(flag,weas)
        # 获取未来10小时的天气情况
        # weaHours = weather.parseNext12HourWeather(soup)
        # msg = u'hello world'
        if weChat.sendMsg(msg, markFriend) is False:
            print u'发送失败'

    '''
    print url
    msg = u'hello world'
    if myChat.sendMsg(msg, markFriend) is False:
        print u'发送失败'
    '''
if __name__ == "__main__":

    # 先登录微信
    myChat = weChat.WeChat()
    if myChat.login() == False:
        print u'登录失败'
        myChat.exit()
    if myChat.getFriends() == False:
        print u'获取好友列表失败'
        myChat.exit()

    markFriend = u'我的机器人'
    url = 'http://www.weather.com.cn/weather/101110101.shtml'
    while True:
        fun_get_weather(myChat,markFriend,url)
        time.sleep(span)

    print u'\n执行完成'
