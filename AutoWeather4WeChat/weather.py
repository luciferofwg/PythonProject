#coding=utf-8


from bs4 import BeautifulSoup
import requests
import re
import json
import datetime,time
import collections

# 定义一个空的类，用来存储天气的信息
class Weather:
    # 日期
    date =""
    # 天气
    weather = ""
    # 温度
    temp = ""
    # 风向
    wind = ""
    # 风速
    windSpeed = ""
    def __init__(self,date,weather,temp,wind,windSpeed):
        self.date = date
        self.weather = weather
        self.temp = temp
        self.wind = wind
        self.windSpeed = windSpeed
    def show(self):
        s = u"日期："+self.date+u"\n天气："+self.weather+u'\n温度：'+self.temp+u'\n风向：'+self.wind+u'\n风速：'+ self.windSpeed
        print s

def witeTmp(data):
    path = u'E:/learn/GitHub/PythonProject/weather/data.txt'
    output = open(path, 'w')
    output.write(data)
    output.close()

def getCurDayAndHour(curDate):
    c = re.findall('\d+',curDate)
    # 年月
    ym = time.strftime("%Y%m", time.localtime(time.time()))
    # 分秒
    ms = time.strftime("%M%S", time.localtime(time.time()))
    return ym + str(c[0]) + str(c[1]) + ms


# 获取页面数据
def getDoc(url):
    try:
        html = requests.get(url).content
        if html is not None:
            soup = BeautifulSoup(html, 'html.parser')
            return soup

    except Exception, e:
        print u'获取数据失败，错误'+str(e)
        return None

# 解析天气状况
def soupDetail(tag):
    # 日期
    date = tag.h1.string
    # log.logger.debug("日期：%s" % date)
    # 天气
    weather = tag.p['title']
    # log.logger.debug("天气：%s"% weather)
    # 温度
    temp = ""
    for string in tag.find('p',class_="tem",).stripped_strings:
        temp += string
    # log.logger.debug("温度：%s" % temp)
    # 风向
    wind = ""
    for w in tag.find('p',class_="win").find_all('span'):
        wind += w['title']
        wind += '/'
    # log.logger.debug("风向：%s" % wind)
    # 风速
    windSpeed= tag.find('p',class_="win").find('i').get_text()
    # log.logger.debug("风速：%s" % windSpeed)

    wea = Weather(
        re.match('\d+',date).group(),
        # date,
        weather,
        temp,
        wind,
        windSpeed)
    return wea

# 第一个获取到的天
minDay = 0

# 解析未来7天的天气状况
def getWeather(soup):
    weas = []
    # 是否已经获取到第一个天
    bFlag = False
    for tag in soup.find_all('li',class_=re.compile("sky skyid lv")):
        wea = soupDetail(tag)
        if bFlag is False:
            minDay = wea.date
            bFlag = True
            global minDay
        weas.append(wea)
    return weas

# 获取当前的天气，温度，风向变化趋势
def getTodayLives(soup):
    try:
        # tKey = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        dic = {}
        dic=collections.OrderedDict()
        data = soup.find(string=re.compile("hour3data")).string
        data = data.replace("var hour3data=", "")
        if data != "":
            # print data
            jsonData = json.loads(data)
            for j in jsonData:
                for j1 in jsonData[j]:
                    if type(j1) == type([]):
                        for j2 in j1:
                            weaList = re.split(',',j2)
                            wea = Weather(
                                weaList[0],
                                # re.match('\d+',weaList[0]).group(),
                                          weaList[2],
                                          weaList[3],
                                          weaList[4],
                                          weaList[5])

                            key = getCurDayAndHour(weaList[0])
                            dic[key] = wea
                    elif type(j1) == type(u"1"):
                        weaList = re.split(',',j1)
                        wea = Weather(
                            weaList[0],
                            # str(re.findall(r'\d{2}\d*',weaList[0])[0])+','+str(re.findall(r'\d{2}\d*',weaList[0])[1]),
                            # re.findall(r'\d{2}\d{2}',weaList[0])[0],
                                      weaList[2],
                                      weaList[3],
                                      weaList[4],
                                      weaList[5])

                        key = getCurDayAndHour(weaList[0])
                        dic[key] = wea
        return dic
    except Exception, e:
        print u'获取当前数据失败，错误'+str(e)
        return None

def cmpTime(firstTime,secondTime):
    f = datetime.datetime.strptime(firstTime,'%Y%m%d%H%M%S')
    s = datetime.datetime.strptime(secondTime,'%Y%m%d%H%M%S')
    return f>s

def parseNext12HourWeather(soup):
    dict = getTodayLives(soup)
    weas = []
    if dict != None:
        next10Hour = datetime.datetime.now() + datetime.timedelta(hours=10)
        strNext10Hour = next10Hour.strftime("%Y%m%d%H%S%M")
        strCurTime = datetime.datetime.now().strftime("%Y%m%d%H%S%M")
        i=0
        sortDict = sorted(dict.keys())
        for key in sortDict:
            if strNext10Hour > key and strCurTime < key :
                weas.append(dict[key])
    return weas

def formatWeather(wea):
    t = time.localtime()
    # 上月
    last_month = t[1]-1 or 12
    # 下月
    next_month= t[1]+1
    if  next_month> 12:
        next_month = 1
    date = str(t[1]) + u'月'+ str(wea.date)+u'日'

    '''
    if wea.date > minDay:# 下月
        date = str(next_month) + u'月'+ str(wea.date)+u'日'
    elif wea.date == minDay:#当月
        date = str(t[1]) + u'月'+ str(wea.date)+u'日'
    else:#上月
        date = str(last_month) + u'月'+ str(wea.date)+u'日'
    '''
    ret = u'温馨提示：\n明天【' + date + u'】' + \
          u' '+ wea.weather + \
          u' ' + wea.temp+u' ' +\
          wea.wind+\
          wea.windSpeed
    print ret

    return ret
