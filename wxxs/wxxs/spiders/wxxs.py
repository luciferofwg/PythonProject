# coding: utf-8
import re
import scrapy
from bs4 import BeautifulSoup
from scrapy.http import Request
from items import WxxsItem

'''
# 编写爬虫
'''
debug_ = False

class MySpider(scrapy.Spider):
    bash_url = 'http://www.txt53.com/html'
    name = "wxxs"

    sub_titles = []
    sub_titles.append('xuanhuan')
    sub_titles.append('wuxiaxianxia')
    sub_titles.append('dushi')
    sub_titles.append('wangyoujingji2')
    sub_titles.append('yanqing')
    sub_titles.append('kehuanxiaoshuo')
    sub_titles.append('xuanyituili')
    sub_titles.append('qita')

    def start_requests(self):
        for sub_title in self.sub_titles:
            url = self.bash_url + '/' + sub_title
            yield Request(url, self.parse)
            if debug_ is True:
                break

    def parse(self, response):
        # 获取当前页面的最大页
        div = BeautifulSoup(response.text, 'lxml').find('div',attrs={'class':'yemian'})
        if div is not None:
            max_num = div.find_all('strong')[0].get_text()
            if debug_ is True:
                max_num = '3'
            bashurl = str(response.url)
            sub = bashurl.split('/')[-1]
            for num in range(1, int(max_num)):
                url = bashurl + '/list_' + self.check_url(sub) + '_' + str(num) + '.html'
                yield Request(url, callback=self.get_name)

    def check_url(self, sub_string):
        current_index = -1
        for index in range(len(self.sub_titles)):
            if self.sub_titles[index] == sub_string:
                current_index = index
                break
        if current_index == 0:
            return '21'
        elif current_index == 1:
            return '29'
        elif current_index == 2:
            return '35'
        elif current_index == 3:
            return '47'
        elif current_index == 4:
            return '58'
        elif current_index == 5:
            return '70'
        elif current_index == 6:
            return '71'
        elif current_index == 7:
            return '45'

    def get_name(self, response):
        divs = BeautifulSoup(response.text, 'lxml').find_all('div', attrs={'class':'xiazai'})
        for div in divs:
            uls = div.find_all('ul')
            for ul in uls:
                lis = ul.find_all('li')
                index = 0
                for li in lis:
                    url = li.find('a')
                    if url is not None and index != 6:
                        url = url.get('href')
                        yield Request(url=url, callback=self.get_preview)                      
                    else:
                        pass
                    index += 1
                # break
            # break

    def get_preview(self, response):

        div = BeautifulSoup(response.text, 'lxml').find('div', attrs={'class':'shuji'})
        if div is not None:
            lis = div.find_all('li')
            if lis is not None:
                info = []
                for li in lis:
                    string = li.get_text().split(u'：')[-1]
                    info.append(string)

                div = BeautifulSoup(response.text, 'lxml').find('div', attrs={'class':'downbox'})
                if div is not None:
                    download_preview_url = div.find('a').get('href')
                    yield Request(download_preview_url, callback=self.get_download_url,
                    meta={
                        'name':     info[0],
                        'type':     info[1],
                        'author':   info[2],
                        'uploader': info[3],
                        'source':   info[4],
                        'size':     info[5],
                        'update_time':info[6],
                        'popularity': int(info[7])
                    })

    def get_download_url(self, response):
        item = WxxsItem()
        div = BeautifulSoup(response.text, 'lxml').find('div', attrs={'class':'shuji'})
        url = ''
        if div is not None:
            download_urls = div.find_all('li')
            download_zip_url = download_urls[0].find('a').get('href')
            download_txt_url = download_urls[1].find('a').get('href')
            url = download_txt_url

        item['url'] = url
        item['name'] = response.meta['name']
        item['type'] = response.meta['type']
        item['author'] = response.meta['author']
        item['uploader'] = response.meta['uploader']
        item['source'] = response.meta['source']
        item['size'] = response.meta['size']
        item['update_time'] = response.meta['update_time']
        item['popularity'] = response.meta['popularity']

        return item