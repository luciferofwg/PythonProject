# -*- coding: utf-8 -*-

MIN_PROXY_COUNT = 10
# header
HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36',
    'Connection': 'keep-alive',
    'Content-Encoding': 'gzip',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
# sec
CHECK_TIME =  600
# checkout url
URL = "http://www.txt53.com/"
# failed times
FAILED_TIMES = 5

TIME_OUT = 2