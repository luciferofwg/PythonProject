# coding: utf-8
import threading
import schedule
import time
import datetime
import requests

from utils import mongo
from utils import log as logger
from utils import progress

import proxy_crawl
import proxy_cfg as cfg
import pymongo

log= logger.Logger('log.log', 1, "proxy").getlog()

class ProxyPool(object):
    def __init__(self):
        mg = mongo.Mongo()
        self.collection = mg.collect
        self.collection.create_index([('ip', pymongo.ASCENDING)], unique=True, sparse=True)
        self.start()

    # Singleton
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            org = super(ProxyPool, cls)
            cls._instance = org.__new__(cls, *args)
        return cls._instance

    def start(self):
        self.crawl_proxy_task(True)

        # def task():
        #     self.check_ip_availability_task()
        #     schedule.every(cfg.CHECK_TIME).seconds.do(self.check_ip_availability_task)
        #     while True:
        #         schedule.run_pending()
        #         time.sleep(1)

        # thread = threading.Thread(target=task)
        # thread.start()

    def drop_proxy(self):
        self.collection.delete_many({})

    def check_ip_availability_task(self):
        log.debug('检查代理有效性')
        cur_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        proxy_list = self.collection.find()
        delete_count = 0
        update_count = 0
        totol_count = 0
        for proxy in proxy_list:
            ip = proxy['ip']
            start_time = time.time()
            try:
                totol_count += 1
                proxy_ip = {'http': 'http://' + ip}
                rep = requests.get(url=cfg.URL, headers=cfg.HEADERS, proxies=proxy_ip, timeout=cfg.TIME_OUT)
                rep.close()

                if rep.status_code ==200:
                    elapsed = round(time.time() - start_time, 4)
                    try:
                        cur_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        self.collection.update_one({'ip':ip},{'$set':{'update_time':cur_time,'response_speed':elapsed,'validity':True}})
                        log.debug(u'更新来自'+ proxy['origin'] + u'的代理ip: ' + ip + u'数据库中标识')
                        update_count += 1
                    except Exception as e:
                        log.error(e)
                        pass
                else:
                    try:
                        self.collection.delete_one({'ip': ip})
                        log.debug(u'来自'+ proxy['origin'] + u'的代理ip: ' + ip + u' 无效，从数据库中删除')
                        delete_count += 1
                    except Exception as e:
                        log.error(e)
                        pass
            except Exception as e:
                try:
                    self.collection.delete_one({'ip': ip})
                    log.debug(u'来自'+ proxy['origin'] + u'的代理ip: ' + ip + u' 无效，从数据库中删除')
                    delete_count += 1
                except Exception as e:
                    log.error(e)
                    pass
        log.debug(u'完成检查,总共 ' + str(totol_count)+ u' 个,删除 ' + str(delete_count) + u' 个,更新 ' + str(update_count) + u' 个')

    def crawl_proxy_task(self, check_num = False):
        if check_num:
            count = self.collection.count()
            if count > cfg.MIN_PROXY_COUNT:
                log.debug(u'数据库中含有%d条记录，无需重新抓取代理ip' % count)
                return
            else:
                log.debug(u'数据库中含有%d条记录，需重新抓取代理ip' % count)
        else:
            log.debug(u'抓取代理ip')
        proxy_list = proxy_crawl.crawl_proxy()
        log.debug(u"开始保存")
        for proxy in proxy_list:
            if not self.collection.find_one({'ip': proxy.ip}):
                self.collection.insert_one(proxy.__dict__)
        log.debug(u"保存结束")

    def add_failed_proxy(self, ip):
        proxy = self.collection.find_one({'ip': ip})
        if proxy is not None:
            failed_count = proxy['failed_count'] + 1
            log.debug(u'ip: %s 已经失败%d次' % (proxy['ip'],failed_count))
            if failed_count < cfg.FAILED_TIMES:
                try:
                    cur_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    self.collection.update_one({'ip': ip}, {"$set": {'update_time': cur_time, 'failed_count': failed_count}})
                except Exception as e:
                    # log.error(e)
                    pass
            else:
                self.collection.delete_one({'ip':ip})

    def random_choice_proxy(self):
        proxy = self.collection.find().sort(
            [("failed_count", pymongo.ASCENDING), ("validity", pymongo.DESCENDING), ("response_speed", pymongo.ASCENDING),
             ("update_time", pymongo.DESCENDING)])
        return proxy[0]['ip']

# 构建一个对象
proxy_pool = ProxyPool()