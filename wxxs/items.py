# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WxxsItem(scrapy.Item):
    # define the fields for your item here like:
    # 小说的名字
    name = scrapy.Field()
    # 作者
    author = scrapy.Field()
    # 小说地址
    url = scrapy.Field()
    # 上传者
    uploader = scrapy.Field()
    # 出处
    source = scrapy.Field()
    # 人气
    popularity = scrapy.Field()
    # 大小
    size = scrapy.Field()
    # 分类
    type = scrapy.Field()
    # 时间 9
    update_time = scrapy.Field()