﻿#-*- coding: UTF-8 -*- 
import re
import urllib
import urllib2
import os
import shutil
import os.path
import stat
from random import Random

import utils
LOG_LINE = '-----------------------------------------------------------'
#获取URL
def getUrl(data):
	afReg = re.compile(r'<a href=.*?"/')
	urlReg0 = re.compile(r'title=.*?>')
	urlReg1 = re.compile(r'"')
	urlReg2 = re.compile(r'.*?>')
	
	result = re.sub(afReg, "", data)
	result = re.sub(urlReg0, "", result)
	result = re.sub(urlReg1, "", result)
	result = re.sub(urlReg2, "", result)
	return result.strip()
	
#获取标题
def getTitle(data):
	afReg = re.compile(r'<a href=.*?="')
	urlReg0 = re.compile(r'target.*?>')
	urlReg1 = re.compile(r'"')
	urlReg2 = re.compile(r'<.*?>')
	
	result = re.sub(afReg, "", data)
	result = re.sub(urlReg0, "", result)
	result = re.sub(urlReg1, "", result)
	result = re.sub(urlReg2, "", result)
	return result.strip()
	
#获取URL和URL标题
def getUrlTitle(data):
	trReg = re.compile(r'<tr bgcolor=.*?>(.*?)</tr>', re.S)
	urlCxtReg = re.compile(r'<a href=".*?(.*?)">')
	urlReg = re.compile(r'')
	cxrReg = re.compile(r'')
	results = re.findall(trReg, data)

	conUrls = []
	conTitles = []
	for result in results:
		urlCxt = re.search(urlCxtReg, result)
		url = getUrl(urlCxt.group())
		title = getTitle(urlCxt.group())
		if url:
			conUrls.append(url)
		if title:
			utils.logger.info('get title: ' + title + ' succ')
			conTitles.append(title)
	return (conUrls, conTitles)

#获取URL对应的HTML数据，并存储到文件中
def getUrlPage(hostUrl, pagePath, title, subUrl):
	try:
		imgFoldeName = 'dat'
		#根据title创建文件夹
		#defultPath = 'G:\\GitHub\\PythonProject\\84_ScrapyAuto\\'
		defultPath = utils.getCurPath()
		#print '#defultPath=' + defultPath	
		foldePath = utils.mkdirFolde(pagePath, imgFoldeName)
		#print '@foldePath = ' + foldePath
		path = foldePath + '\\' +title + '.dat'
		#print 'path: ' + path
		url = hostUrl + subUrl
		utils.logger.info('download data from url: ' + url)

		req_timeout = 8#默认时间5s
		request = urllib2.Request(url)
		response = urllib2.urlopen(request, None, req_timeout)
		
		html = response.read()
		utils.writeData(path, html, "a")
		
	except IOError, error:
		utils.logger.info("download error: %s error %s" % (url, error))
	except Exception, e:
		utils.logger.info("Exception :" + str(e))
		
#遍历当前页面的url，并下载数据
def traversTutle(hostUrl, pagePath, tutleUT):
	urls = tutleUT[0]
	titles = tutleUT[1]
	size = 0
	if len(urls) <= len(titles):
		size = len(urls)
	else:
		size = len(titles)
	utils.logger.info('download urls:')
	for index in range(0,size):
		getUrlPage(hostUrl, pagePath, titles[index], urls[index])
