﻿#-*- coding: UTF-8 -*- 
import re
import urllib
import urllib2
import stat
import os
import shutil
import os.path
import stat
import logging
import threading
import time

#导入亚洲
import catch_asia
#导入图片处理模块
import catch_pic
#导入utils公共模块
import utils
#导入日志模块
LOG_LINE = '-----------------------------------------------------------'

#线程类
class myThread (threading.Thread):   #继承父类threading.Thread
	def __init__(self, catchWay, threadID, hostUrl, threadTitle, threadUrl):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.hostUrl = hostUrl
		self.threadTitle = threadTitle
		self.threadUrl = threadUrl
		self.catchWay = catchWay
		
	def run(self):				   #把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
		utils.logger.info('begin catch ' + self.threadTitle )
		#调用线程函数
		traversPicCxtUrl(self.catchWay, self.hostUrl, self.threadID, self.threadTitle, self.threadUrl)
		utils.logger.info('compete catch ' + self.threadTitle )

#创建线程，并且启动线程
def createThread(catchWay, threadCount, hostUrl, threadTitles, threadUrls):
	#判断传参是否满足条件
	utils.logger.info('threadCount :' + str(threadCount))
		
	if threadCount == len(threadTitles) and threadCount == len(threadUrls):
		threads = []
		for index in xrange(0, threadCount):
			# 创建新线程
			time.sleep(0.1)
			thread = myThread(catchWay, index, hostUrl, threadTitles[index], threadUrls[index])
			utils.logger.info('create thread  for get ' + threadTitles[index] + 'succ')
			thread.start()
			threads.append(thread)  
		#等待线程运行结束
		for t in threads:  
			t.join()
		
#获取当前页面编号
def getCurPageNum(data):
	divReg = re.compile('<div class="page">(.*?)</div>', re.S)
	strongReg = re.compile('<strong(.*?)/strong>')
	repReg = re.compile('<.*?>')
	
	result = re.search(divReg, data)
	result = re.search(strongReg, result.group(0))
	result = re.sub(repReg, "", result.group(0))
	return result


def replaceTailNum(index, tailString):
	subUrl = ''
	subReg = re.compile('.*?/16_', re.S)
	if 0 == index:
		subUrl = '/16_'
		subReg = re.compile('.*?/16_', re.S)
	elif 1 == index:
		subUrl = '/17_'
		subReg = re.compile('.*?/17_', re.S)
	elif 2 == index:
		subUrl = '/18_'
		subReg = re.compile('.*?/18_', re.S)
	elif 3 == index:
		subUrl = '/19_'
		subReg = re.compile('.*?/19_', re.S)
	elif 4 == index:
		subUrl = '/20_'
		subReg = re.compile('.*?/20_', re.S)
	elif 5 == index:
		subUrl = '/21_'
		subReg = re.compile('.*?/21_', re.S)
	elif 6 == index:
		subUrl = '/22_'
		subReg = re.compile('.*?/22_', re.S)
	elif 7 == index:
		subUrl = '/23_'
		subReg = re.compile('.*?/23_', re.S)
	htmlReg = re.compile('.html.*', re.S)
	
	tailNum = re.sub(subReg, "", tailString)
	tailNum = re.sub(htmlReg, "", tailNum)
	return tailNum
	
#获取最大页面编号
def getMaxPageNum(iCount, data):
	divReg = re.compile(r'<div class="page">(.*?)</div>', re.S)
	afReg = re.compile(r'<a href="(.*?)</a>', re.S)
	tailReg = re.compile(r'.*?(尾页)$', re.S)
	
	str16 = str(iCount) + '_'
	tailReg0 = re.compile(r'')
	results = re.search(divReg, data)
	#print results.group(1)
	if results:
		results = re.findall(afReg, results.group(1))
		for index in range(0, len(results)):
			test = re.match(tailReg, results[index])	
			if test:
				#print test.group()
				tailNum = replaceTailNum(iCount, test.group())
				return tailNum

	
#获取标题
def getPicEnters(data):
	ulReg = re.compile(r'<ul class=".*?>(.*?)</ul>', re.S)
	afReg = re.compile(r'<a href=(.*?)</a>')
	urlReg = re.compile(r'.*?>')
	tiltleReg = re.compile(r'.*?">')
	rmvReg = re.compile(r'"/|">')
	contUrls = []
	contTitles = []
	#print data
	results = re.findall(ulReg, data)

	if results:
		tilurlS = re.findall(afReg,results[1])
		for index in range(0, len(tilurlS)):
			urls = re.findall(urlReg, tilurlS[index])
			url = re.sub(rmvReg, "", urls[0])
			title = re.sub(tiltleReg, "", tilurlS[index])
			if url and title:
				contUrls.append(url)
				contTitles.append(title)
		return contTitles,contUrls
	else:
		return None

#获取主页的内容
def getHostPage(url):
	try:
		utils.logger.info("getHostPage url=" + url)
		request = urllib2.Request(url)
		response = urllib2.urlopen(request)
		return response.read()

	except IOError, error:
		utils.logger.info("download error: %s error %s" % (url, error))
	except Exception, e:
		utils.logger.info("Exception :" + str(e))
	'''	
	except urllib2.URLError, e:
		if hasattr(e,"reason"):
			print u"get host page failed. reason is :",e.reason
	'''
			
#抓取当前页面url所有内容
def getCurPageData(filePath, hostUrl, data, pageNum):
	#创建页面文件夹
	utils.logger.info('getCurPageData path = ' + filePath + str(pageNum))
	pagePath = utils.mkdirFolde(filePath, str(pageNum))
	#解析当前页面的数据
	utils.logger.info(LOG_LINE)
	utils.logger.info('prase asia data')
	results = catch_asia.getUrlTitle(data)
	catch_asia.traversTutle(hostUrl, pagePath, results)
	utils.logger.info(LOG_LINE)
	utils.logger.info('prase asia data over')

	#下载当前页面的图片
	utils.logger.info(LOG_LINE)
	utils.logger.info('prase picUrl and download imgs')
	data = catch_pic.traverFiles(pagePath)
	#utils.clearFiles(pagePath)

#重组分页url
def reloadPageUrl(index, curUrl, page):
	utils.logger.info('index=' + str(index) + ',curUrl=' + curUrl + ',page=' + str(page))
	subUrl = ''
	subReg = re.compile('/16.*?\.html', re.S)
	if 0 == index:
		subUrl = '/16_'
		subReg = re.compile('/16.*?\.html', re.S)
	elif 1 == index:
		subUrl = '/17_'
		subReg = re.compile('/17.*?\.html', re.S)
	elif 2 == index:
		subUrl = '/18_'
		subReg = re.compile('/18.*?\.html', re.S)
	elif 3 == index:
		subUrl = '/19_'
		subReg = re.compile('/19.*?\.html', re.S)
	elif 4 == index:
		subUrl = '/20_'
		subReg = re.compile('/20.*?\.html', re.S)
	elif 5 == index:
		subUrl = '/21_'
		subReg = re.compile('/21.*?\.html', re.S)
	elif 6 == index:
		subUrl = '/22_'
		subReg = re.compile('/22.*?\.html', re.S)
	elif 7 == index:
		subUrl = '/23_'
		subReg = re.compile('/23.*?\.html', re.S)
	
	
	newSub = subUrl + str(page) + '.html'
	#print 'newSub = ' + newSub
	newUrl = re.sub(subReg, newSub, curUrl)
	#print 'newUrl = ' + newUrl
	return newUrl

#处理单个图区的信息
def dealSinglePic(catchWay, filePath, iPic, data, url, hostUrl):
	#爬取方式，1顺序，0倒序
	if 0 == int(catchWay):#倒序抓取数据
		#解析数据，获取当前的页面，以供遍历所有的页面
		maxPageNum = getMaxPageNum(iPic, data)	
		#获取当前页的页码
		index = int(maxPageNum)
		while index >= 0:
			if 1!=index:#获取亚洲区第二分页内容
				utils.logger.info('dealSinglePic ' + 'indx=' + str(index)+ ', iPic=' + str(iPic) + ',url=' + url)
				newUrl = reloadPageUrl(iPic, url,index)
				utils.logger.info('dealSinglePic newUrl=' + newUrl)
				data = getHostPage(hostUrl+newUrl)
				utils.logger.info(LOG_LINE)
				utils.logger.info('threadId ' + str(index)+ ' get next page data')
				utils.logger.info('threadId ' + str(index)+ ' newUrl=' + newUrl)
			getCurPageData(filePath, hostUrl, data, index)
			index -= 1
			utils.writeData(filePath, str(index), 'a')
			
	elif 1 == int(catchWay):
		#解析数据，获取当前的页面，以供遍历所有的页面
		curPageNum = getCurPageNum(data)
		#获取当前页的页码
		index = int(curPageNum)
		maxPage = 10#getMaxPageNum(iPic, data)#最大页面数
		while index < maxPage:
			if 1!=index:#获取亚洲区第二分页内容
				newUrl = reloadPageUrl(iPic, url,index)
				data = getHostPage(hostUrl+newUrl)
				utils.logger.info(LOG_LINE)
				utils.logger.info('threadId ' + str(index)+ ' get next page data')
				utils.logger.info('threadId ' + str(index)+ ' newUrl=' + newUrl)
			utils.logger.info('getCurPageData filePath ' + filePath)
			utils.logger.info('getCurPageData hostUrl ' + hostUrl)
			utils.logger.info('getCurPageData index ' + str(index))
			getCurPageData(filePath, hostUrl, data, index)
			index += 1
			#utils.writeData(filePath, str(index), 'a')
	else:
		utils.logger.info('catch way is illegal')
def traversPicCxtUrl(catchWay, hostUrl, index, title, url):
	#创建图区文件夹
	path = utils.getCurPath()
	filePath = utils.mkdirFolde(path, title)
	#获取当前图区的内容
	data = getHostPage(hostUrl + url)
	utils.logger.info(LOG_LINE)
	utils.logger.info('get current page data')
	dealSinglePic(catchWay, filePath, index, data, url, hostUrl)

#遍历图片区每一个图区Title和URL
def traversPicCxtUrls(catchWay, hostUrl, results):
	titles = results[0]
	urls = results[1]
	
	size = 0
	if len(urls) <= len(titles):
		size = len(urls)
	else:
		size = len(titles)
	for index in range(0,size):
		traversPicCxtUrl(catchWay, hostUrl, index, titles[index], urls[index])

#顺序爬还是倒序爬
def catchByOrder():
	utils.logger.info('please input catch  the way by order(1) or reOrder(0)')
	num = raw_input("")
	while num==1 or num ==0:
		break;
	return num

#打印图片区的title及url，供用户选择抓取	
def printTitleAndChooseArea(hostUrl, results):
	catchWay = catchByOrder()
	titles = results[0]
	urls = results[1]
	
	size = 0
	if len(urls) <= len(titles):
		size = len(urls)
	else:
		size = len(titles)
	for index in range(0,size):
		if titles[index] and urls[index]:
			utils.logger.info('num : ' + str(index) + '\tTilte: ' + titles[index] + '\t Url: ' + hostUrl + urls[index])
	utils.logger.info('please input nums you want catch (0~' + str(index) + ') :')
	num = raw_input("")
	if 0 <= int(num) and int(num) <= size:
		utils.logger.info('only catch title=' + titles[int(num)])
		traversPicCxtUrl(catchWay, hostUrl, num, titles[int(num)], urls[int(num)])
	else:
		utils.logger.info('catch by orders(0~' + str(index) + ')')
		#traversPicCxtUrls(hostUrl, results)	
		#为所有的图区创建线程
		createThread(catchWay, size, hostUrl, titles, urls)
#代码主体
def main():	
	hostUrl = 'http://www.84ki.com/'
	#获取主页的内容
	utils.logger.info(LOG_LINE)
	utils.logger.info('get host data')
	utils.logger.info('host url:' + hostUrl)
	data = getHostPage(hostUrl)
	#utils.writeData('d:\\data.dat', data, "a")
	#data = utils.readData("d:\\data.dat", "r")
	
	utils.logger.info(LOG_LINE)
	utils.logger.info('get pic area url&title')
	#获取亚洲区的内容
	picTitleUrls = getPicEnters(data)
	#遍历图片区每一个图区和URL
	utils.logger.info(LOG_LINE)
	utils.logger.info('travers area urls')
	#traversPicCxtUrls(hostUrl, picTitleUrls)
	printTitleAndChooseArea(hostUrl, picTitleUrls)
if __name__ == "__main__":
	main()
else:
	print "called from intern."


