﻿#-*- coding: UTF-8 -*- 
import re
import urllib
import urllib2
import os
import shutil
import os.path
import stat
from random import Random

import utils
LOG_LINE = '-----------------------------------------------------------'

class GetPic:
	def __init__(self,foldePath, timeOut=10):
		self.foldePath =foldePath
		self.timeOut = timeOut
		
	#获取标题
	def getTitle(self,data):
		titleDiv = re.compile('<div class="title">(.*?)</div>', re.S)
		titleReg = re.compile('<div class="title">|</div>')
		result = re.search(titleDiv, data)
	
		if result:
			title = re.sub(titleReg, "", result.group(0))
			return title
			utils.logger.info('title:' + title)
		else:
			return utils.getRandomName()
			
	#获取图片url
	def getImgUrl(self,data):
		imgReg = re.compile('<img src="(.*?.jpg)')
		results = re.findall(imgReg, data)
	
		contents = []
		for result in results:
			contents.append(result)
		return contents
		
	#根据图片url下载图片
	def downloadImg(self,imgPath, iCount, imgURL):
		try:
			#print 'imgURL=' + imgURL
			urlopen=urllib.URLopener()
			#伪装浏览器
			req_header = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
							'Accept':'text/html;q=0.9,*/*;q=0.8',
							'Accept-Charset':'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
							'Accept-Encoding':'gzip',
							'Connection':'close',
							'Referer':None #注意如果依然不能抓取的话，这里可以设置抓取网站的host
						}
			req_timeout = self.timeOut#默认时间10s
			req = urllib2.Request(imgURL, None, req_header)
			fp = urllib2.urlopen(req, None, req_timeout)
			data = fp.read()
			fp.close()
			
			fileName = imgPath + '\\' + str(iCount) + ".jpg"
			file = open(fileName, "w+b")
			file.write(data)
			file.close()
		except IOError, error:
			utils.logger.info("download error: %s error %s" % (imgURL, error))
		except Exception, e:
			utils.logger.info("Exception :" + str(e))
	
	#进度统计
	def countProcess(self,curCount, TotalCount):
		return curCount/float(TotalCount)*100
		
	#分析数据且下载图片
	def parseDownload(self,foldePath, data):
		titleName = getTitle(data)
		#print 'titleName\t:' + titleName
		imgPath = utils.mkdirFolde(foldePath, titleName)
		results = getImgUrl(data)
		utils.logger.info('parse url:' + str(len(results)))
		utils.logger.info(LOG_LINE)
		utils.logger.info('Download imgs begin...')
		index = 0
		bFlag = False
		for imgUrl in results:
			index += 1
			downloadImg(imgPath, index, imgUrl)
		utils.logger.info(LOG_LINE)
		utils.logger.info('Download imgs complete')
	#遍历foldePath下的所有文件，并且读取文件中的数据
	def traverFiles(self,foldePath):
		#print 'foldePath=' + foldePath
		for parent,dirnames,filenames in os.walk(foldePath):
			for filename in filenames:
				datPath = os.path.join(parent,filename)
				data = utils.readData(datPath, "r")
				parseDownload(foldePath, data)
	


	

