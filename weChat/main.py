#coding=utf-8
import weChat
import time
import sys
if __name__ == "__main__":
    myChat = weChat.WeChat()
    if myChat.login() == False:
        print u'登录失败'
        myChat.exit()
    if myChat.getFriends() == False:
        print u'获取好友列表失败'
        myChat.exit()

    print u'请输入用户的备注名：'
    remarkName = raw_input("").decode(sys.stdin.encoding)
    while 1:
        print u'请输入要发送的信息：'
        inputMsg = raw_input("").decode(sys.stdin.encoding)
        if myChat.sendMsg(inputMsg, remarkName) is False:
            print '发送失败'
        time.sleep(1)

    myChat.exit()

    print u'\n执行完成'