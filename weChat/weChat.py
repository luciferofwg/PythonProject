#coding=utf-8

import itchat
import time
import re
import threading

from friend import Friend
from itchat.content import *

# 存储收到的每一条消息
msg_information = {}
# 存储被撤回的消息，线程检测并发送
msg_recall = []


# 接收时间的回调函数
@itchat.msg_register([TEXT, PICTURE, FRIENDS, CARD, MAP, SHARING, RECORDING, ATTACHMENT, VIDEO],
                     isFriendChat=True,
                     isGroupChat=True,
                     isMpChat=True)
def handleReceiveMsg(msg):
    try:
        # 接受消息的时间
        msg_time_rec = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        #在好友列表中查询发送信息的好友昵称
        # msg_from = itchat.search_friends(userName=msg['FromUserName'])['NickName']
        msg_time = msg['CreateTime']    #信息发送的时间
        msg_id = msg['MsgId']    #每条信息的id
        msg_content = None      #储存信息的内容
        msg_share_url = None    #储存分享的链接，比如分享的文章和音乐

        msgTyp = msg['Type']
        msgId = msg['MsgId']

        #如果发送的消息是文本或者好友推荐
        if msgTyp == 'Text' or msgTyp == 'Friends':
            msg_content = msg['Text']
            print msg['FromUserName'], u"文本消息：", msg_content

        #如果发送的消息是附件、视频、图片、语音
        elif msgTyp == "Attachment" or msgTyp == "Video" or msgTyp == 'Picture' or msgTyp == 'Recording':
            # 以下代码会自动回复给对方
            # typeSymbol = {
            #     PICTURE: 'img',
            #     VIDEO: 'vid', }.get(msg.type, 'fil')
            # return '@%s@%s' % (typeSymbol, msg.fileName)

            msgType = msg.type
            outStr = ""
            path = ""
            if "Picture" == msgType:
                path = './pic/'
                outStr = u"图片消息："
            elif "Recording" == msgType:
                path = './voice/'
                outStr = u"语音消息："
            elif "Attachment" == msgType:
                path = './attach/'
                outStr = u"附件消息："
            elif "Video" == msgType:
                path = './video/'
                outStr = u"视频消息："

            msg.download(path+msg.fileName)
            print msg['FromUserName'], u"发来" , outStr, msg.fileName

        #如果消息是推荐的名片
        elif msgTyp == 'Card':
            msg_content = msg['RecommendInfo']['NickName'] #昵称
            if msg['RecommendInfo']['Sex'] == 1:
                msg_content += u'男'
            else:
                msg_content += u'女'
            print msg_content

        #如果消息为分享的位置信息
        elif msgTyp == 'Map':
            x, y, location = re.search(
                "<location x=\"(.*?)\" y=\"(.*?)\".*label=\"(.*?)\".*", msg['OriContent']).group(1, 2, 3)
            if location is None:
                #内容为详细的地址
                msg_content = u"纬度->" + x.__str__() + u" 经度->" + y.__str__()
            else:
                msg_content = u"" + location
            print msg['FromUserName'], u"发来位置共享消息：", msg_content
        #如果消息为分享的音乐或者文章，详细的内容为文章的标题或者是分享的名字
        elif msgTyp == 'Sharing':
            msg_content = msg.FileName
            msg_share_url = msg.Url
            print msg['FromUserName'], u"发来共享链接消息：", u'\n标题：',msg_content, u'\n链接', msg_share_url

        msg_information.update({
            msg_id: {
                "msg_from": msg['FromUserName'],
                "msg_time": msg_time,
                "msg_time_rec": msg_time_rec,
                "msg_type": msg["Type"],
                "msg_content": msg_content,
                "msg_share_url": msg_share_url
            }
        })

    except Exception,e:
        print u'解析消息异常：错误：' + str(e)


@itchat.msg_register(NOTE, isFriendChat=True, isGroupChat=True, isMpChat=True)
def checkReCall(msg):
    if u'撤回了一条消息' in msg['Content']:
        #在返回的content查找撤回的消息的id
        old_msg_id = re.search("\<msgid\>(.*?)\<\/msgid\>", msg['Content']).group(1)
        #得到消息
        old_msg = msg_information.get(old_msg_id)
        if old_msg is not None:
            msg_body = None
            if old_msg['msg_type'] == "Sharing":
                print old_msg.get('msg_from') ,u' 撤回了一个分享\n标题为：',old_msg.get('msg_content'),u'\n链接为：',old_msg.get('msg_share_url')
                msg_body = old_msg.get('msg_from') + u' 撤回了一个分享,' + u'\n标题为：' + old_msg.get('msg_content') + u'\n链接为：',old_msg.get('msg_share_url')
            elif old_msg['msg_type'] == "Text":
                print old_msg.get('msg_from'),u' 撤回了一条消息,内容为：' + old_msg.get('msg_content')
                msg_body = old_msg.get('msg_from') + u' 撤回了一条消息,' + u'内容为：' + old_msg.get('msg_content')
            # 删除旧数据
            msg_information.pop(old_msg_id)


class WeChat:
    # 私有变量，是否登陆成功
    __bLogin = False
    # 所有的好友
    __allFriends = []
    # 标记的重点好友
    __markFriend = []
    # 重点标记好友的名称，需要手动维护
    __markFriendsList = [u'我的机器人',u'邓芬']

    def __init__(self):
        pass

    # 登录网页版微信
    def login(self):
        itchat.auto_login(hotReload=True,loginCallback=self.__loginCallBack,exitCallback=self.__exitCallBack)

        # print u'2222222'
        # t = threading.Thread(target=self.__run, name='LoopThread')
        # t.start()
        # t.join()
        # print u'111111111111'

    def getFriends(self):
        if self.__bLogin:
            # 获取好友列表
            friends = itchat.get_friends(update=True)[1:]
            # print friends
            # 解析好友
            if self.__parseFriends(friends):
                print u'发现' + str(len(self.__allFriends)) + u'个好友'
                # 标记重点好友
                if self.__checkMarkFriend() is False:
                    print u'标记重点好友失败'
                    return False
                # 启动接收消息功能
                itchat.run(debug=False,blockThread=False)
                return True
            else:
                print u'解析好友失败'
                return False
        else:
            print u'登录失败，无法获取到好友信息'
            return False

    # 根据备注名查找用户的信息
    def getUserbyRemarkName(self,remarkName):
        for friend in self.__allFriends:
                if friend.remarkname == remarkName:
                    return friend,True
        return None,False

    def sendMsg(self,msg, remarkName):
        if self.__bLogin:
            user,ok = self.getUserbyRemarkName(remarkName)
            if ok is False:
                print u'未找到备注为 ' + remarkName + u' 的用户'
                return False
            try:
                itchat.send_msg(msg=msg, toUserName=user.name)
                return True
            except Exception,e:
                print u'发送失败，错误：' + str(e)
                return False
        else:
            print u'尚未登录，请登录'
            return False
    def exit(self):
        itchat.logout()
        self.__bLogin = False

    def __run(self):
        while 1:
            print len(msg_recall)
            time.sleep(3)

            '''
            if len(msg_recall) >0:
                threading.Lock()
                for i in range(len(msg_recall)):
                    try:
                        self.sendMsg(msg_recall.pop(-1), toUserName='filehelper')
                    except Exception,e:
                        print u'发送消息失败'
            '''


    # 登录成功的回调函数
    def __loginCallBack(self):
        print u"登录成功"
        self.__bLogin = True

    # 退出的回调函数
    def __exitCallBack(self):
        print u"强制退出成功"
        self.__bLogin = False

    # 是否已经登录
    def isLogin(self):
        return self.__bLogin

    # 解析好友列表
    def __parseFriends(self,friends):
        try:
            for friend in friends:
                name = friend['UserName']
                nickName = friend['NickName']
                remarkname = friend['RemarkName']
                if remarkname==u'':
                    remarkname = u'无备注'
                s = friend['Sex']
                sex = ""
                if s==1:
                    sex = u'男'
                elif s==2:
                    sex = u'女'
                else:
                    sex = u'未知'
                province = friend['Province']
                city = friend['City']
                location = province+u'-'+city
                signature = friend['Signature']

                f = Friend(name, nickName, remarkname, sex, location, signature)
                self.__allFriends.append(f)
            return True
        except Exception,e:
            print u'解析好友失败，错误：' + str(e)
            return False

    # 标记重点好友
    def __checkMarkFriend(self):
        for friend in self.__allFriends:
            for remarkName in self.__markFriendsList:
                if friend.remarkname == remarkName:
                    self.__markFriend.append(friend)
        return True

    def __getRamarkNameByUserName(self,username):
        for f in self.__allFriends:
            if f.name==username:
                return f.remarkname
        return None