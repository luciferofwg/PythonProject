#coding=utf-8
class Friend:
    # 姓名
    name =""
    # 昵称
    nickName = ""
    # 备注名称
    remarkname = ""
    # 性别
    sex = ""
    # 地区
    location = ""
    def __init__(self,name,nickName,remarkname,sex,location,signature):
        self.name = name
        self.nickName = nickName
        self.remarkname = remarkname
        self.sex = sex
        self.location = location
        self.signature = signature
    def show(self):
        try:
            s = u"姓名："+self.name+\
                u"\n备注："+self.remarkname+\
                u'\n性别：'+self.sex+\
                u'\n位置：'+self.location+\
                u'\n个性签名：'+self.signature
            print u'----------------\n'
            print s
        except Exception,e:
            print u"打印失败，当前用户名："+ self.name+u"错误" + str(e)

