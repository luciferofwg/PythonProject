#-*- coding: UTF-8 -*- 
import urllib
import urllib2
import os
import os.path
import shutil
from random import Random
import zipfile
import sys,time
import gzip
import log

#获取当前文件绝对路径
def getCurPath():
	return os.getcwd()

FILENAME = getCurPath() + '\\' + 'log.log'
LOG_LINE = '-----------------------------------------------------------'
#初始化日志模块
logger = log.Logger(FILENAME, 1, "root").getlog()
