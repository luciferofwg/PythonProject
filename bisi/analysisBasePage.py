#-*- coding: UTF-8 -*-
import utils
import log
import re
def parsePageSubUrl(data):
	Urls = []
	# 提取url的正则表达式
	ulClass = re.compile(r'<ul class="p_pop h_pop"(.*?)</ul>', re.S)
	subUrl = re.compile(r'"(.*?)>')
	results = re.findall(ulClass, data)
	index = 1
	for result in results:
		urls = re.findall(r'<a href="(.*?>)',result)
		for url in urls:
			if None is not url:
				url = re.sub(r'<a href="','',url)
				url = re.sub(subUrl,'',url)
				if index == 1 or index == 2:
					Urls.append(url)
					index = index + 1

	return  Urls