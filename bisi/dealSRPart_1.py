#-*- coding: UTF-8 -*-
import utils
import requests
import re
import log

def parsePage(data):
	utils.logger.debug('parsePage ')

def getPageByUrl(url):
	utils.logger.debug('getPageByUrl:'+ url)
	path = utils.getCurPath() + '\\bisiSRPart1.dat'
	debug = utils.debug
	if debug is False:
		header = {
			'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
			'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
		try:
			r = requests.post(url,headers = header,timeout=utils.timeout)
			r.raise_for_status()
		except requests.RequestException as e:
			utils.logger.debug('getPageByUrl failed, error msg:' + str(e))
		else:
			# 获取到数据
			utils.writeData(path,r.content,'w')
			return r.content
	else:
		data = utils.readData(path,'r')
		return data

def dealSRPage(baseUrl, subUrl):
	url = baseUrl + subUrl
	# 获取当前页面的数据
	data = getPageByUrl(url)
	parsePage(data)
