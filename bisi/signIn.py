#-*- coding: UTF-8 -*-
import utils
import log
import requests
import myTimer as mt
'''
#
# 解析数据
# 1.找到每日签到选择的表情，并选择
# 2.提交敲到
#
# ###########################################
# 遇到的问题
# 1.页面操作时，需要在页面上使用鼠标点击一个心情
# 2.选中心情之后才能点击签到的按钮
# 3.无法在只拿取到页面html的时候选择心情，选择不
# 到心情就无法点击签到按钮
#
# ##########################################
# 正常页面上点击的情况：
# 1.点击‘签到’按钮之后，一共产生9个请求
# 2.其中有一个请求就是对签到的请求
# 3.目前的策略是直接忽略其他的请求，只关注签到请求
#
#
'''
def prasePage(data):
	# 创建定时器
	mt.createTimer()
	# 取消定时器
	# mt.cancelTimer()

def getPageContext(url):
	debug = utils.debug
	path = utils.getCurPath() + '\\bisiSignIn.dat'
	if debug is False:
		header = {
			'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
			'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
		try:
			r = requests.post(url,headers = header,timeout=utils.timeout)
			r.raise_for_status()
		except requests.RequestException as e:
			utils.logger.debug('getPageContext failed, error msg:' + str(e))
		else:
			# 将数据存储到本地，用于离线解析测试
			utils.writeData(path, r.content, 'w')
			return r.content
	else:
		data = utils.readData(path, 'r')
		return data

def signInPerDay(baseUrl,subUrl):
	url = baseUrl + subUrl
	utils.logger.debug('signInPerDay:' + url)
	data = getPageContext(url)
	prasePage(data)

