#-*- coding: UTF-8 -*-
import utils
import requests
import re
import log

import dealSRPart_1 as srp1


def filterUrl(title):
	constContext = ['東方美人(BT)',
	                '東方美人(高清BT)',
	                '東方美人(BT合集區)',
	                '東方美人(免空)',
	                '東方美人(迅雷/電驢)']
	flag = False
	for context in constContext:
		if title == context:
			flag = True
			break
	return  flag


def parsePageContent(data):
	debug = utils.debug
	# 提取url的正则表达式
	divReg = re.compile(r'<div id="category_1.*?>(.*?)</table>.*?</div>', re.S)
	trReg = re.compile(r'<tr>(.*?)</tr>', re.S)
	urlReg = re.compile(r'<a href=(.*?)</a>',re.S)
	titles = []
	urls = []

	results = re.search(divReg,data)
	if results is not None:
		for trString in re.findall(trReg, results.group()):
			for url in re.findall(urlReg, trString):
				if -1 != url.find('.html',0, len(url)):
					title = re.search(r'alt="(.*?)>',url)
					url = re.search(r'"(.*?)>',url)
					if title is not None and url is not None:
						urlTemp = re.sub(r'"|/>|>','',url.group())
						titleTemp = re.sub(r'"|/>|alt=','',title.group())
						# if debug is True:
						# 	path = utils.getCurPath() + '\\bisiurltitle.dat'
						# 	# if True == filterUrl(titleTemp):
						# 	tmp = urlTemp+',\t'+titleTemp+'\n'
						# 	utils.writeData(path,tmp,'a')

						titles.append(titleTemp)
						urls.append(urlTemp)

		return titles,urls

def getPageContent(url,debug):
	header = {
		'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
		'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
	try:
		r = requests.post(url,headers = header,timeout=utils.timeout)
		r.raise_for_status()
	except requests.RequestException as e:
		utils.logger.debug('getPageContent failed, error msg:' + str(e))
	else:
		if debug is False:
			# 已经获取到数据，开始解析数据
			return r.content
		else:
			path = utils.getCurPath() + '\\bisiSR.dat'
			utils.writeData(path,r.content,'w')
			return None

def dealBisiSR(baseUrl,subUrl):
	url = baseUrl + subUrl
	utils.logger.debug('dealBisiSR:'+ url)
	debug = utils.debug
	# 获取特区主页内容
	data = getPageContent(url,debug)
	if debug is True:
		path = utils.getCurPath() + '\\bisiSR.dat'
		data = utils.readData(path,'r')
	# 解析页面数据
	titles,urls = parsePageContent(data)
	index = 1
	print ('*******************************************')
	for title in titles:
		print '**   ' + str(index) + '.  '+ title.decode('utf-8')
		index = index + 1
	print ('*******************************************')
	utils.logger.info('please input numbers you want choice:(0~' + str(len(titles)) + ') :')
	num = raw_input("")
	while 0 > int(num) or int(num) > len(titles):
		utils.logger.info('please input numbers you want choice:(0~' + str(len(titles)) + ') :')
		num = raw_input("")
	utils.logger.info('get ' + titles[int(num)].decode('utf-8') + ' pages')

	# 特区第一部分数据
	srp1.dealSRPage(baseUrl,urls[int(num)])