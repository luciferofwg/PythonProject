#-*- coding: UTF-8 -*-
import threading
import time
import datetime
# import signIn
import utils
import requests

flag = False

def messionFunc():
	debug = utils.debug
	path = utils.getCurPath() + '\\bisiSignResult.dat'
	if debug is False:
		url = 'http://hk-pic1.xyz/plugin.php?id=dsu_paulsign:sign&operation=qiandao&infloat=1&inajax=1'
		header = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Referer': ' http://hk-pic1.xyz/plugin.php?id=dsu_paulsign:sign',
			'Content-Type': 'application/x-www-form-urlencoded'}
		try:
			r = requests.post(url, headers=header, timeout=utils.timeout)
			r.raise_for_status()
		except requests.RequestException as e:
			utils.logger.debug('requestSign failed, error msg:' + str(e))
		else:
			flag = True
			utils.logger.info('requestSign sucessed, status is:' + str(r.status_code))
	else:
		flag = True
		utils.logger.info('requestSign excuted. ')

def createTimer():
	timer = threading.Timer(1, funcTimer)
	timer.start()

def cancelTimer():
	timer.cancel()


def funcTimer():
	if flag is False:
		beginTime = '09:00:00'
		endTime = '18:55:00'
		checkTimeSpan = 1 # 单位s

		curTimeString = datetime.datetime.now().strftime('%H:%M:%S')
		if -1 == cmp(beginTime, curTimeString) and -1 == cmp(curTimeString, endTime):
			if flag is False:
				# 做定时做的任务
				messionFunc()
		global timer
		timer = threading.Timer(checkTimeSpan, funcTimer)
		timer.start()
	else:
		cancelTimer()
