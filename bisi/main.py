#-*- coding: UTF-8 -*-

import utils
import log
import requests
import analysisBasePage as abp
import signIn as sign
import dealSR as sr
import sys

def choicePage(baseUrl,urls):
	print ('*******************************************')
	print '**   1.  ' + '每日签到'.decode('utf-8')
	print '**   2.  ' + '比思特区'.decode('utf-8')
	print ('*******************************************')
	utils.logger.info('please input numbers you want choice:(1~2) :')
	num = raw_input("")
	while 0 > int(num) or int(num) > 2:
		utils.logger.info('please input numbers you want choice:(1~2) :')
		num = raw_input("")
	if 1 == int(num):
		utils.logger.info('deal ' + '【每日签到】'.decode('utf-8') + ' pages')
		# 每日签到 url
		# url = baseUrl + urls[0]
		sign.signInPerDay(baseUrl,urls[0])
	elif 2 == int(num):
		utils.logger.info('deal' + '【比思特区】'.decode('utf-8') + ' pages')
		# 比思特区 url
		# url = baseUrl + urls[1]
		sr.dealBisiSR(baseUrl,urls[1])

def getBasePage(debug):
	path = utils.getCurPath() + '\\bisi.dat'
	baseUrl = 'http://hk-pic1.xyz/'
	if debug is False:
		header = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
			'Content-Type': 'application/x-www-form-urlencoded'}

		try:
			r = requests.post(baseUrl,headers = header,timeout=utils.timeout)
			r.raise_for_status()
		except requests.RequestException as e:
			utils.logger.debug('getBasePage failed, error msg:' + str(e))
		else:
			urls = abp.parsePageSubUrl(r.content)
			# 将数据存储到本地，用于离线解析测试
			utils.writeData(path,r.content,'w')
			choicePage(baseUrl,urls)
	else:
		data = utils.readData(path,'r')
		urls = abp.parsePageSubUrl(data)
		choicePage(baseUrl,urls)


def login(debug):
	if debug is False:
		url = 'http://hk-pic1.xyz/member.php?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&inajax=1'
		header = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
		payload = {
			'fastloginfield': 'username',
			'username': 'jszh',
			'password': 'Jszh@2016',
			'quickforward': 'yes',
			'handlekey': 'lsl'}
		try:
			r = requests.post(url, headers=header, data=payload, timeout=utils.timeout)
			r.raise_for_status()
		except requests.RequestException as e:
			utils.logger.debug('login failed, error msg:' + str(e))
		else:
			# 登录成功，之后显示的是当前的主页 url = http://hk-pic1.xyz/
			utils.logger.debug('login sucessed')
			getBasePage(debug)
	else:
		getBasePage(debug)

def main():
	debug = utils.debug
	login(debug)
	getBasePage(debug)

if __name__ == "__main__":

	main()
else:
	print "called from intern."