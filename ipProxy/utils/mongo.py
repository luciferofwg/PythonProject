# coding: utf-8
import pymongo
import datetime
import mongo_cfg as config

class utils:
    def sort_dict(self, src_dict, reverse=True):
        dts_dict = None
        #用lambda表达式来排序，更灵活：
        if reverse is False:#升序
            dts_dict= sorted(src_dict.items(), lambda x, y: cmp(x[1], y[1]))
        else:#降序
            dts_dict = sorted(src_dict.items(), lambda x, y: cmp(x[1], y[1]), reverse=True)
        return dts_dict

    def prt_dict(self,src_dict):
        for k,v in src_dict.items():
            print k,':',str(v)

    def prt_list(self, src_list, prt_count = 10):
        index = 1
        for key, value in src_list:
            if index > prt_count:
                break
            index += 1
            fmt = '%s:%s' % (key,value)
            print fmt


class Mongo(object):
    '''成员变量'''
    # 对象
    client = None
    db = None
    # 集合对象
    collect = None
    # 工具类
    utils = utils()

    # 成员函数
    def __init__(self):
        self.client = pymongo.MongoClient(host=config.cfg['MONGO_HOST'], port=config.cfg['MONGO_PORT'])
        self.db = self.client[config.cfg['MONGO_DB']]  # 获得数据库的句柄
        self.collect = self.db[config.cfg['MONGO_COLL']]  # 获得collection的句柄       

    # 查询所有的数据的数目
    def get_count_collect(self):
        if self.collect is not None:
            return self.collect.find().count()
        else:
            return 0

    # 查询特定字段的所有不重复的值
    def query_field_array(self, field):
        results= self.collect.distinct(field)
        return results
    
    # 统计特定字段的个数
    def count_fields(self, fields, conditions):
        if len(fields) != len(conditions):
            return 0

        result_dict = {}
        for index_fields in range(0, len(fields)):
            result_dict[fields[index_fields]] = conditions[index_fields]
        if self.collect is not None:
            return self.collect.find(result_dict).count()
        else:
            return 0

    # 删除特定字段的数据
    def remove_fields(self, fields, conditions):      
        if len(fields) != len(conditions):
            return 0
        result_dict = {}
        for index in range(0, len(fields)):
            result_dict[fields[index]] = conditions[index]

        if self.collect is not None:
            return self.collect.remove(result_dict)
    
    # 统计特定字段不同值的个数
    # 
    # 某个作者的作品个数
    # field         ：author
    # condition     ：辰东，唐家三少。天蚕土豆
    # count_result  ：n1,n2,n3
    def count_multi_field(self, field, conditions):     
        if len(conditions) == 0:
            return 0
        count_result = {}
        for index in range(0, len(conditions)):
            field_tmp = []
            field_tmp.append(field)
            condition_tmp = []
            condition_tmp.append(conditions[index])
            count_result[conditions[index]] = self.count_fields(field_tmp, condition_tmp)
        # 按照值排序
        result_dst = self.utils.sort_dict(count_result)
        # 打印
        self.utils.prt_list(result_dst)

    
    # 查询特定字段的所有不重复的值
    def query_field_array(self, field):     
        if self.collect is not None:
            results= self.collect.distinct(field)
            return results
        else:
            return 0

    # 查询人气且排序,默认降序
    def query_sort_popularity(self, field, orderby=-1, count_out=3):       
        results = None
        if self.collect is not None:
            results = self.collect.find({},{'popularity':1, 'name':1, 'author':1,'update_time':1,'uploader':1,'_id':0 }).sort([(field,orderby)]).limit(count_out)
        else:
            return None

        print '人气榜（前' + str(count_out) + ')名：'
        for index in range(0, count_out):
            head = 'No.' + str(index+1) + '\tpopularity:' + str(results[index]['popularity'])
            name = '\tname:'+results[index]['name']
            author = '\tauthor:'+results[index]['author']
            update_time = '\tupdate_time:'+results[index]['update_time']
            print head,name,author,update_time
        print '-----------------'

    # 插入函数
    def insert_data(self, data):  
        if self.collect is not None:
            self.collect.insert(data)
        else:
            return 0