#coding=utf-8

import city
import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')
from bs4 import BeautifulSoup
import requests

def getPage(citycode):
	# url= "http://www.weather.com.cn/data/cityinfo/"+citycode+".html"#构造网址
	url = 'http://www.weather.com.cn/weather1d/101011100.shtml'
	html = requests.get(url).content
	return html

def soupTest():
	html_doc = """
<html>
	<head>
		<title>The Dormouse's story</title>
	</head>
	<body>
		<p class="title">
			<b>The Dormouse's story</b>
		</p>

		<p class="story">Once upon a time there were three little sisters; and their names were
			<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
			<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
			<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>

		<p class="story">...</p>
"""

	soup = BeautifulSoup(html_doc, 'html.parser')
	# print soup.prettify()
	print soup.title
	print soup.title.name
	print soup.title.string
	print soup.title.parent
	print soup.title.parent.name


	print "+++++++++++++++++++"
	print soup.p
	# print soup.find_all('p')
	# print soup.head
	print soup.p['class']

	print soup.a
	for link in soup.find_all('a'):
		print link.get('href')

	print soup.find(id="link3")
	print soup.get_text()
	print "+++++++++++++++++++"

	head_tag=soup.head
	print head_tag.contents
	title_tag=head_tag.contents[0]
	# print title_tag.contents

	print soup.contents[0].name

    # 遍历字符串，包含空格或者空行
	for string in soup.strings:
		print string

	print "*************"
	# 遍历字符串，除去了空白行及空格等
	for string in soup.stripped_strings:
		print string

	# 父节点
	# .parents
	print "+++++++++++++++++++"
	print soup.a
	for parent in soup.a.parents:
		if parent is None:
			print parent
		else:
			print parent.name


	print "+++++++++++++++++++"
	print soup.find_all(['a','b'])

	print "+++++++++++++++++++"
	print soup.find_all(has_class_but_no_id)


def has_class_but_no_id(tag):
    return tag.has_attr('class') and not tag.has_attr('id')


def soupTest1():
	html='<b class="boldest">Extremely bold</b>'
	soup = BeautifulSoup(html,'html.parser')
	tag= soup.b
	print type(tag)
	print tag.name
	print tag.attrs
	print tag['class']
	print tag.get('class')

def soupTest2():
	html='<p class="body strikeout"></p>'
	css_soup   = BeautifulSoup(html,'html.parser')
	tag= css_soup.p
	print tag.name
	print tag['class']

def soupTest3():
	html='<p>Back to the <a rel="index">homepage</a></p>'
	rel_soup = BeautifulSoup(html,'html.parser')
	print rel_soup.a['rel']
	# 修改了p属性的值
	rel_soup.a['rel']=['index','contents']
	print rel_soup.p

def soupTest4():
	html='<p>Back to the <a rel="index">homepage</a></p>'
	rel_soup = BeautifulSoup(html,'html.parser')
	print rel_soup.a['rel']
	# 修改了p属性的值
	rel_soup.a['rel']=['index','contents']
	print rel_soup.p

def soupTest5():
	html = '<a><b>text1</b><c>text2</c></b></a>'
	sibling_soup =BeautifulSoup(html,'html.parser')
	print sibling_soup.prettify()
	print sibling_soup.b.next_sibling
	print sibling_soup.b.previous_sibling

if __name__ == "__main__":
	soupTest()
	# soupTest1()
	# soupTest2()
	# soupTest3()
	# soupTest4()
	# soupTest5()

	# citycode = ''
	# ctn=getPage(citycode)
	# soup = BeautifulSoup(ctn, 'html.parser',from_encoding='utf-8')
	# # print soup.title
	# print soup.find_all("div", class_="tem").span

#
# 	try:
# 		cityname = raw_input("please input city you want?")
# 	except:
# 		print "cityname Not Found"
# 	if cityname:
# 		try:
# 			citycode = city[cityname]
# 			print getPage(citycode)
# 		except:
# 			print "citycode Not Found"
# else:
	print "called from intern."