#-*- coding: UTF-8 -*- 
'''
import multiprocessing
import time

def worker_1(interval):
    print "worker_1"
	print interval
    for index in range(0, interval):
		time.sleep(0.01)
    print "end worker_1"

def worker_2(interval):
    print "worker_2"
    print interval
	for index in range(0, interval):
		time.sleep(0.01)
    print "end worker_2"

def worker_3(interval):
    print "worker_3"
	print interval
    for index in range(0, interval):
		time.sleep(0.01)
    print "end worker_3"

if __name__ == "__main__":
    p1 = multiprocessing.Process(target = worker_1, args = (2,))
    p2 = multiprocessing.Process(target = worker_2, args = (3,))
    p3 = multiprocessing.Process(target = worker_3, args = (4,))

    p1.start()
    p2.start()
    p3.start()

    print("The number of CPU is:" + str(multiprocessing.cpu_count()))
	
    for p in multiprocessing.active_children():
        print("child   p.name:" + p.name + "\tp.id" + str(p.pid))
    print "END!!!!!!!!!!!!!!!!!"
'''

#! /usr/bin/env python
# -*- coding:utf-8   -*-
# __author__ == "tyomcat"
# "我的电脑有4个cpu"
'''
from multiprocessing import Pool
import os, time

def long_time_task(name):
    print 'Run task %s (%s)...' % (name, os.getpid())
    start = time.time()
    time.sleep(3)
    end = time.time()
    print 'Task %s runs %0.2f seconds.' % (name, (end - start))

if __name__=='__main__':
    print 'Parent process %s.' % os.getpid()
    p = Pool()
    for i in range(4):
        p.apply_async(long_time_task, args=(i,))
    print 'Waiting for all subprocesses done...'
    p.close()
    p.join()
    print 'All subprocesses done.'
'''
'''
import multiprocessing
import os, time
def worker(taskName,url, path, pageNum):
	"""thread worker function"""
	print 'Run taskName=%s, pid=(%s)...' % (taskName, os.getpid()) 
	print 'max=', max
	print 'path=', path
	print 'pageNum=', pageNum
	return

if __name__ == '__main__':
	jobs = []
	url = 'www.baidu.com'
	path = 'd:\\test.py'
	pageNum = 5
	
	
	for i in range(3):
		p = multiprocessing.Process(target=worker, args=(i, url, path, pageNum))
		jobs.append(p)
		p.start()
'''

import multiprocessing
import time
import myLog

def func(msg,url, path, pageNum):
	myLog.writeLog('d',"msg:" + msg)
	myLog.writeLog('d','max=' + max)
	myLog.writeLog('d','path='+ path)
	myLog.writeLog('d','pageNum=' + pageNum)
	
	time.sleep(0.1)
	myLog.writeLog('d',"end")
	return "done" + msg
 
if __name__ == "__main__":
	myLog.InitLog()
	pool = multiprocessing.Pool(processes=4)
	result = []
	for i in xrange(3):
		msg = "hello %d" %(i)
		url = 'www.baidu.com'
		path = 'd:\\test.py'
		pageNum = 5
		result.append(pool.apply_async(func, (msg, url, path, pageNum)))
	pool.close()
	pool.join()
	for res in result:
		print ":::", res.get()
	myLog.writeLog('d',"Sub-process(es) done.")
	
	


































