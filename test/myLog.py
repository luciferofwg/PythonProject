#-*- coding: UTF-8 -*- 
import logging
import os
import os.path
import shutil

def InitLog():
	FILENAME = os.getcwd() + '\\' + 'LOG.log'
	SHOWFORMATTER = '%(asctime)s %(filename)-4s[line:%(lineno)d]: %(levelname)-8s %(message)s'
	LOGGFORMATTER = '%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)-8s %(message)s'

	#定义日志的格式
	logging.basicConfig(level=logging.DEBUG,
					format=LOGGFORMATTER,
					#datefmt='%a, %d %b %Y %H:%M:%S',
					filename=FILENAME,
					filemode='a')

	#定义日志在屏幕上显示级别
	console = logging.StreamHandler()
	console.setLevel(logging.INFO)
	formatter = logging.Formatter(SHOWFORMATTER)
	console.setFormatter(formatter)
	logging.getLogger('').addHandler(console)

def writeLog(logLevel, logMsg):
	if logLevel == 'e':
		logging.error(logMsg)
	if logLevel == 'd':
		logging.debug(logMsg)
	elif logLevel == 'w':
		logging.warning(logMsg)
	elif logLevel == 'i':
		logging.info(logMsg)
