#coding=utf-8


from bs4 import BeautifulSoup
import requests
import re
import json
import datetime,time
import collections

# 定义一个空的类，用来存储天气的信息
class Weather:
    # 日期
    date =""
    # 天气
    weather = ""
    # 温度
    temp = ""
    # 风向
    wind = ""
    # 风速
    windSpeed = ""
    def __init__(self,date,weather,temp,wind,windSpeed):
        self.date = date
        self.weather = weather
        self.temp = temp
        self.wind = wind
        self.windSpeed = windSpeed
    def show(self):
        s = u"日期："+self.date+u"\n天气："+self.weather+u'\n温度：'+self.temp+u'\n风向：'+self.wind+u'\n风速：'+ self.windSpeed
        print s

def witeTmp(data):
    path = u'E:/learn/GitHub/PythonProject/weather/data.txt'
    output = open(path, 'w')
    output.write(data)
    output.close()

def getCurDayAndHour(curDate):
    c = re.findall('\d+',curDate)
    # 年月
    ym = time.strftime("%Y%m", time.localtime(time.time()))
    # 分秒
    ms = time.strftime("%M%S", time.localtime(time.time()))
    return ym + str(c[0]) + str(c[1]) + ms


# 获取页面数据
def getDoc(url):
    try:
	    return requests.get(url).content
    except Exception, e:
        print u'获取数据失败，错误'+str(e)
        return None

# 解析天气状况
def soupDetail(tag):
    # 日期
    date = tag.h1.string
    # log.logger.debug("日期：%s" % date)
    # 天气
    weather = tag.p['title']
    # log.logger.debug("天气：%s"% weather)
    # 温度
    temp = ""
    for string in tag.find('p',class_="tem",).stripped_strings:
        temp += string
    # log.logger.debug("温度：%s" % temp)
    # 风向
    wind = ""
    for w in tag.find('p',class_="win").find_all('span'):
        wind += w['title']
        wind += '/'
    # log.logger.debug("风向：%s" % wind)
    # 风速
    windSpeed= tag.find('p',class_="win").find('i').get_text()
    # log.logger.debug("风速：%s" % windSpeed)

    wea = Weather(
        re.match('\d+',date).group(),
        # date,
        weather,
        temp,
        wind,
        windSpeed)
    return wea

# 解析未来7天的天气状况
def getWeather(soup):
    weas = []
    for tag in soup.find_all('li',class_=re.compile("sky skyid lv")):
        wea = soupDetail(tag)
        weas.append(wea)
    return weas

# 获取当前的天气，温度，风向变化趋势
def getTodayLives(soup):
    try:
        # tKey = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        dic = {}
        dic=collections.OrderedDict()
        data = soup.find(string=re.compile("hour3data")).string
        data = data.replace("var hour3data=", "")
        if data != "":
            # print data
            jsonData = json.loads(data)
            for j in jsonData:
                for j1 in jsonData[j]:
                    if type(j1) == type([]):
                        for j2 in j1:
                            weaList = re.split(',',j2)
                            wea = Weather(
                                weaList[0],
                                # re.match('\d+',weaList[0]).group(),
                                          weaList[2],
                                          weaList[3],
                                          weaList[4],
                                          weaList[5])

                            key = getCurDayAndHour(weaList[0])
                            dic[key] = wea
                    elif type(j1) == type(u"1"):
                        weaList = re.split(',',j1)
                        wea = Weather(
                            weaList[0],
                            # str(re.findall(r'\d{2}\d*',weaList[0])[0])+','+str(re.findall(r'\d{2}\d*',weaList[0])[1]),
                            # re.findall(r'\d{2}\d{2}',weaList[0])[0],
                                      weaList[2],
                                      weaList[3],
                                      weaList[4],
                                      weaList[5])

                        key = getCurDayAndHour(weaList[0])
                        dic[key] = wea
        return dic
    except Exception, e:
        print u'获取当前数据失败，错误'+str(e)
        return None

def cmpTime(firstTime,secondTime):
    f = datetime.datetime.strptime(firstTime,'%Y%m%d%H%M%S')
    s = datetime.datetime.strptime(secondTime,'%Y%m%d%H%M%S')
    return f>s

def parseNext12HourWeather(soup):
    dict = getTodayLives(soup)
    weas = []
    if dict != None:
        next10Hour = datetime.datetime.now() + datetime.timedelta(hours=10)
        strNext10Hour = next10Hour.strftime("%Y%m%d%H%S%M")
        strCurTime = datetime.datetime.now().strftime("%Y%m%d%H%S%M")
        i=0
        sortDict = sorted(dict.keys())
        for key in sortDict:
            if strNext10Hour > key and strCurTime < key :
                weas.append(dict[key])
    return weas

if __name__ == "__main__":
    url = 'http://www.weather.com.cn/weather/101110101.shtml'
    html = getDoc(url)
    if html is not None:
        witeTmp(html)
        # 构造bs对象
        soup = BeautifulSoup(html, 'html.parser')

        print u'---------------\n未来7天的天气情况：\n---------------'
        # 此处获取到的数据是按照日期有序的
        weas1 = getWeather(soup)

        for w in weas1:
            print '******'
            w.show()

        print u'---------------\n未来10小时各时间点的天气情况：\n---------------'
        # 此处获取到的数据是按照日期有序的
        weas2 = parseNext12HourWeather(soup)

        for w in weas2:
            print '******'
            w.show()
    else:
        print(u'没有获取到页面内容')

    print u'\n执行完成'