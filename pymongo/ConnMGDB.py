#-*- coding: UTF-8 -*-
from pymongo import MongoClient

# 初始化客户端
def InitClient(ip, port):
	if ip and 0 == port:
		client = MongoClient(ip, port)
	else:
		client = MongoClient('localhost', 27017)
	return client

# 连接数据库
def connectDB(client):
	#connect db
	db = client.runoob
	if db:
		return db
	else:
		print 'connectDB db is None'
		return None

# 根据索引查找数据
def findCollect(db, index):
	condition = ''
	collection = db.runoob
	if collection:
		for item in collection.find():
			print item
	else:
		print 'findDBCollect collection is None'

# 查找所有的数据
def findAllCollect(db):
	collection = db.runoob
	if collection:
		for item in collection.find():
			print item
	else:
		print 'findDBCollect collection is None'

# 给数据库中写入数据
def insert2DB(db, data):
	collection = db.test
	if collection:
		collection.insert(data)
	else:
		print 'insert2DB collection is None'



# 测试函数
def testMongoDB():
	# 初始化连接
	client = InitClient('',0)
	db = connectDB(client)
	#insert data to pydb
	for index in range(0,5):
		if db:
			data = {}
			data['index'] = str(index)
			data['name'] = '斗破苍穹'
			data['url'] = 'www.baidu.com'
			data['size'] = '3.1M'
			data['author'] = '天蚕土豆'
			data['date'] = '2017-7-11 11:42:02'

			insert2DB(db, data)

	#select all data in collection
	if db:
		print 'datas in collection:'
		findAllCollect(db)