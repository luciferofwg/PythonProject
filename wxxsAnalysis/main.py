#!/usr/bin/python
# coding: utf-8

import operate as mongo
import time
import datetime

debug_ = False

def run():
    mg = mongo.mongo_operate()

    print '总数据：', mg.get_count_collect()
    print '-----------------'

    if debug_ is False:
        print '类型按数量由高到低：\n'
        start = datetime.datetime.now()
        results = mg.query_field_array( 'type')
        field = 'type'
        mg.count_multi_field( field, results)
        end = datetime.datetime.now()
        print '\n统计', str(len(results)),'个数据，历时', (end-start).total_seconds(),'s\n-----------------'

        print '作者按数量由高到低：\n'
        start = datetime.datetime.now()
        # 作者的名字
        results = mg.query_field_array( 'author')
        # 根据作者名字查询每个作者的作品 数量，
        field = 'author'
        mg.count_multi_field( field, results)
        end = datetime.datetime.now()
        print '\n统计', str(len(results)),'个数据，历时', (end-start).total_seconds(),'s\n-----------------'

        print '上传者按数量由高到低：\n'
        start = datetime.datetime.now()
        results = mg.query_field_array( 'uploader')
        field = 'uploader'
        mg.count_multi_field( field, results)
        end = datetime.datetime.now()
        print '\n统计', str(len(results)),'个数据，历时', (end-start).total_seconds(),'s\n-----------------'

        print '来源按数量由高到低：\n'
        start = datetime.datetime.now()
        results = mg.query_field_array( 'source')
        field = 'source'
        mg.count_multi_field( field, results)
        end = datetime.datetime.now()
        print '\n统计', str(len(results)),'个数据，历时', (end-start).total_seconds(),'s\n-----------------'

    # mg.query_sort_popularity( 'popularity',count_out=10)

if __name__ == "__main__":
    # print '\n\n************统计*************\n'
    # run()

    index = 1
    while True:
        print '\n\n************统计*************\n'
        curent_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print curent_time,' 当前统计第',index,'次'
        run()
        time.sleep(60)
        index += 1
else:
    print "called from intern."